# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2023.0.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2023.0.2...v2023.0.3) (2023-01-02)


### Bug Fixes

* Versionsinkompatibilität bei mPDF ([108a09f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/108a09f1f575b58dcd2e279d5fd312b094143d3f))

### [2023.0.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2023.0.1...v2023.0.2) (2023-01-02)


### Bug Fixes

* PDF-Ausdrucke sind nicht mehr möglich ([3ff3ff6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3ff3ff66f8f0f71da384402b9b1a42b848d50d66))

### [2023.0.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2023.0.0...v2023.0.1) (2023-01-02)


### Bug Fixes

* Backups enden mit Fehlermeldung ([d8e2039](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d8e2039a217b0d45f1109072fa0789ffea15fb84))
* Beerdigung kann nicht gespeichert werden, wenn bei einer Kalenderverbindung Fehler auftreten. ([dbc868d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/dbc868dc4cc338a5544883830d3a449d6277bf03))
* Fehler beim Import vom EventKalender ([46a7c62](https://codeberg.org/pfarrplaner/pfarrplaner/commits/46a7c6279eb7e358598fe452f1fde4cf16e66ca7))
* Liturgie lässt sich nicht mehr öffnen ([49f9c20](https://codeberg.org/pfarrplaner/pfarrplaner/commits/49f9c2093dfbc405c9355a8dd6ec1263cf48bf34))

## [2023.0.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.164.1...v2023.0.0) (2023-01-01)


### Bug Fixes

* Fehler bei der Ausgabe, wenn Liedtitel leer ist ([a21b0c1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a21b0c171b8102805d9d30eddc014908bb884cd3))
* Zu viele Fehlerreports wegen Spamanfragen ([afcb269](https://codeberg.org/pfarrplaner/pfarrplaner/commits/afcb269480e046e6f3ffd22ec65da8f91396676a))

### [2022.164.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.164.0...v2022.164.1) (2022-12-30)


### Bug Fixes

* Fehler beim Konvertieren von Benutzereinstellungen ([1481659](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1481659c09fe19428834bf53cddb5cc2b6ddd56c))

## [2022.164.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.163.0...v2022.164.0) (2022-12-30)


### Features

* Alte Verschlüsselung entfernt ([0257e7e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0257e7e0c6aebca8e0ccd759986481ddf7b1c44f))
* Neuer Verschlüsselungsstandard für persönliche Daten ([ecdb4bd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ecdb4bd7256f1b6aa0f8308731c8dc61f89e5e78))

## [2022.163.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.162.1...v2022.163.0) (2022-12-27)


### Features

*  Ersetze alle liturgischen Textelemente durch Freitexte mit dem entsprechenden Text ([7b7c529](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7b7c529884b85acb78cfaff540f839d63c5ee9e2)), closes [#345](https://codeberg.org/pfarrplaner/pfarrplaner/issues/345)
*  Führe Ersetzungen in liturgischen Texten direkt im Editor durch ([2f17683](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2f176834d9d40339e74cc887c2d0096eb8ecd710)), closes [#346](https://codeberg.org/pfarrplaner/pfarrplaner/issues/346)
*  Predigtelement muss zur Auswahl der Predigt geöffnet werden ([1192d33](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1192d33831bff2f569216fe2a3047c63832b7f83)), closes [#348](https://codeberg.org/pfarrplaner/pfarrplaner/issues/348)
*  Verwende einen formattierenden Editor (Quill) für FreetextItemEditor ([fc039b8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/fc039b87765229efb2853cce7dba5ab5697e9174)), closes [#344](https://codeberg.org/pfarrplaner/pfarrplaner/issues/344)
* Bearbeiten von verantwortlichen Personen direkt beim Bearbeiten eines Elements ([c1800eb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c1800eb0ef3ddc5c7b9b90ac737e564fa7c001f5)), closes [#347](https://codeberg.org/pfarrplaner/pfarrplaner/issues/347)
* Bei Dienstanfrage auch Basisdienste (P/O/M) erlauben ([5cd558a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5cd558a715740ad52548df744f2c1a8cd102bde6)), closes [#352](https://codeberg.org/pfarrplaner/pfarrplaner/issues/352)
* Dienstplan für einzelne Dienste jetzt auch als Exceltabelle ([00dc9a0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/00dc9a07f10d1f81b235c337cc469ea5a82af8b9))
* Erste Textbausteine für Trauungen und Taufen ([606679d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/606679dc16b3b2be42def659b498812d71eccb3d))
* Kleine Layoutverbesserungen ([e0583e5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e0583e5f75fb57d14ea70366c596eba94829ed7d))
* Kleine Layoutverbesserungen ([b647d94](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b647d94a88224be4018d45b278b3d0230f7cf377))
* Verbesserungen bei der Auswahl von liturgischen Texten ([b0244bf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b0244bf045d759f62f616862a3d1e7a0434bdda9))
* Verbesserungen bei der Predigtauswahl ([9f7dea2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9f7dea2c695a1e709ef7053281add00dd3053617))


### Bug Fixes

* Exception handler berücksichtigt keine Ausnahmen ([8aa7b51](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8aa7b519a34db7b7cdd0f9f99fb0166f2b41abee))
* Fehler bei der Normalisierung von Personennamen ([f2f9583](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f2f9583377cf46c5a5373835d5764b29b37b5bf0))
* Kleine Layoutkorrekturen ([967cb80](https://codeberg.org/pfarrplaner/pfarrplaner/commits/967cb808aa70a683df3cb4bac984df787b4c4548))
* Korrekturen an der Höhe von Dialogen ([c618f83](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c618f83ab42ccf2e2c2c062f9db54093394df155))
* Liedauswahl wird nicht gespeichert ([2f29df4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2f29df4031b5f71963973dfc5d63ae4cef8fdd7e))
* Obsolete Schaltfläche entfernt ([36c392b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/36c392b8325ecac0ba51afe3563535f02b214d4c))
* Quill-Editor akzeptiert keine Zeilenumbrüche mehr ([eac3217](https://codeberg.org/pfarrplaner/pfarrplaner/commits/eac3217e6d71649ccf8a3d5996d7b22336f8a2d1))
* Quill-Editor ist nicht korrekt konfiguriert ([df1869a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/df1869aad0f863fd219fbb9ca37af224c812291e))

### [2022.162.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.162.0...v2022.162.1) (2022-12-15)


### Bug Fixes

* Menü für Beerdigungs-bezogene Texte funktioniert nicht, wenn Elemente leer sind ([8c39ea2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8c39ea2c14d71febe7c293a0c9a4180054b6a9cf))

## [2022.162.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.161.0...v2022.162.0) (2022-12-15)


### Features

* Einfügen von Texten zur Beerdigung bei Freitextelementen in der Liturgie ([8118649](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8118649b314709fd42d6563930b10b8243cc4406))


### Bug Fixes

* Downloadmenü für Liturgie fehlt ([692818c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/692818c77cf050e8ab80954f4e27256e97531016)), closes [#343](https://codeberg.org/pfarrplaner/pfarrplaner/issues/343)

## [2022.161.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.159.0...v2022.161.0) (2022-11-28)


### Features

* Allow inserting Bible text into FreetextItems ([c690252](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c6902520fa2a74daa0ea53aa5d2f857b2e5f09f9)), closes [#338](https://codeberg.org/pfarrplaner/pfarrplaner/issues/338)
* Always use full book names for pericopes ([4faffd6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4faffd6a24c84f6b32ee266f829fdcab8e4d259c)), closes [#335](https://codeberg.org/pfarrplaner/pfarrplaner/issues/335)
* Larger drop area for file drag-and-drop upload ([c348afe](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c348afe8353019d99d9a6f2c88b3eb487a6120f5))
* List scripture reading in SongMail ([59abf20](https://codeberg.org/pfarrplaner/pfarrplaner/commits/59abf2097fbbb45527c14f6eb9e8d555d333ec9c)), closes [#339](https://codeberg.org/pfarrplaner/pfarrplaner/issues/339)
* Provide export for sermon only ([b41ee00](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b41ee00b2fdc0974eeae695fcbc9c6786411f3ca)), closes [#301](https://codeberg.org/pfarrplaner/pfarrplaner/issues/301)


### Bug Fixes

* New person modal is not scrollable (and therefore not saveable) in Edge ([4e453bf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4e453bf7f7a5927a54845d477cfd9c2fbe347abb)), closes [#336](https://codeberg.org/pfarrplaner/pfarrplaner/issues/336)

## [2022.160.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.159.0...v2022.160.0) (2022-11-28)


### Features

* Allow inserting Bible text into FreetextItems ([c690252](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c6902520fa2a74daa0ea53aa5d2f857b2e5f09f9)), closes [#338](https://codeberg.org/pfarrplaner/pfarrplaner/issues/338)
* Always use full book names for pericopes ([4faffd6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4faffd6a24c84f6b32ee266f829fdcab8e4d259c)), closes [#335](https://codeberg.org/pfarrplaner/pfarrplaner/issues/335)
* Larger drop area for file drag-and-drop upload ([c348afe](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c348afe8353019d99d9a6f2c88b3eb487a6120f5))
* List scripture reading in SongMail ([59abf20](https://codeberg.org/pfarrplaner/pfarrplaner/commits/59abf2097fbbb45527c14f6eb9e8d555d333ec9c)), closes [#339](https://codeberg.org/pfarrplaner/pfarrplaner/issues/339)
* Provide export for sermon only ([b41ee00](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b41ee00b2fdc0974eeae695fcbc9c6786411f3ca)), closes [#301](https://codeberg.org/pfarrplaner/pfarrplaner/issues/301)


### Bug Fixes

* New person modal is not scrollable (and therefore not saveable) in Edge ([4e453bf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4e453bf7f7a5927a54845d477cfd9c2fbe347abb)), closes [#336](https://codeberg.org/pfarrplaner/pfarrplaner/issues/336)

## [2022.159.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.158.2...v2022.159.0) (2022-11-25)


### Features

* Add default ministries per city ([4e95cb1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4e95cb1a09fbf7e2eb95d8cec21221bb5609cf1b))

### [2022.158.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.158.1...v2022.158.2) (2022-11-25)


### Bug Fixes

* Plan for organists reverses final psalm verses ([e9aa024](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e9aa0245f759749be969607c14e591f14fa979ff)), closes [#337](https://codeberg.org/pfarrplaner/pfarrplaner/issues/337)

### [2022.158.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.158.0...v2022.158.1) (2022-11-22)


### Bug Fixes

* Ministries not editable in PeopleTab ([7bca47b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7bca47bac8f4bf893ed2286ad0e1f0d1fec10c3f))

## [2022.158.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.157.1...v2022.158.0) (2022-11-18)


### Features

* directly call liturgy / sermon editors from calendar ([afa59f6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/afa59f6bf43330ff11dd6bcd0cc1f564a6fb0c1a))

### [2022.157.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.157.0...v2022.157.1) (2022-11-17)


### Features

* Add more indices for speed gains ([0f1e4e4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0f1e4e486c4697f95ea3db814c933910a859f16b))
* Faster-loading ServiceEditor ([3143dd8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3143dd86ded948e3c4a35f9963cfdeabb46fa9fa))
* Faster-loading ServiceEditor ([5dda994](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5dda994aab995e4002a863182ffe389587df0f40))


### Bug Fixes

* Source list doesn't include past services ([77239db](https://codeberg.org/pfarrplaner/pfarrplaner/commits/77239dbd7c99fd3fb8f6dffe540338657a624ca3))

## [2022.157.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.156.1...v2022.157.0) (2022-11-17)


### Features

* Allow non-toggling quick-assign per API ([5d24041](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5d24041ba9602d735e752f663d16b3c0f092e1b2))
* Improve SongMailLiturgySheet ([94f2ab0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/94f2ab0792669e34a6c61e5e9c35ab226aa0a735)), closes [#330](https://codeberg.org/pfarrplaner/pfarrplaner/issues/330)
* New OrganistLiturgySheet ([45ee256](https://codeberg.org/pfarrplaner/pfarrplaner/commits/45ee25689036e6fa4f29103ce0304ecfd755347e))


### Bug Fixes

* altEG replacing causes some exports to fail ([e26b675](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e26b67517b4e1dbdea1be6a9cb105b664b503c9b))
* Choosing responsible person reloads liturgyEditor ([422951d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/422951d31b92bde54cd397a7be196ff014cc700d)), closes [#329](https://codeberg.org/pfarrplaner/pfarrplaner/issues/329)

### [2022.156.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.156.0...v2022.156.1) (2022-11-15)


### Bug Fixes

* Cannot add new items to LiturgyTree ([0d2ac0c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0d2ac0c6d3ebbdedb3aa245c1b6e43177dfab229))
* dropdown menu doesn't always work in ReadingEditor ([c62f7d4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c62f7d49a8cf7a1db8a9a52549974f6c1fe3589d))
* Song editor fails when initialized without a song ([cfa5a1e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cfa5a1ecb8d1e8aa319f79332547a6b3b4f7e6cf))

## [2022.156.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.155.0...v2022.156.0) (2022-11-14)


### Features

* Import Word document into FreetextEditor ([750e2df](https://codeberg.org/pfarrplaner/pfarrplaner/commits/750e2dffe339931c247dbbe490110aa8dfdb1ceb))

## [2022.155.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.154.2...v2022.155.0) (2022-11-14)


### Features

* Allow loading a liturgic text into a FreetextItem ([85c13ad](https://codeberg.org/pfarrplaner/pfarrplaner/commits/85c13ad19587b80ee540384d13d2c03856cdb7e0)), closes [#289](https://codeberg.org/pfarrplaner/pfarrplaner/issues/289)
* Faster saving on song updates in LiturgyTree ([91d4869](https://codeberg.org/pfarrplaner/pfarrplaner/commits/91d4869061c57d4f5600e06452edda9d2131e034))
* Faster song editor in LiturgyTree ([0301484](https://codeberg.org/pfarrplaner/pfarrplaner/commits/03014840ed8904d2a83183850b8ad9c5c7393a96))
* Songs can now have a reference to an EG number ([4261073](https://codeberg.org/pfarrplaner/pfarrplaner/commits/42610738b0acb5027e7efd4a7dcb4e23f6a5a91d))
* Songs can now have a reference to an EG number ([0262023](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0262023804c30d9abb75327663a346c24f91a92e))

### [2022.154.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.154.1...v2022.154.2) (2022-11-14)


### Features

* Only load songlist once in LiturgyTree ([0ad9162](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0ad9162506b997ee174afaffd6bb846587b44d3a))


### Bug Fixes

* LiturgyEditor does not save Item data correctly ([880d2dc](https://codeberg.org/pfarrplaner/pfarrplaner/commits/880d2dc0c9b6a5f1c3f66e673dcd8cb7f7649aa9))

### [2022.154.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.154.0...v2022.154.1) (2022-11-14)


### Features

* Browse calendar without page reload ([b0a4b3d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b0a4b3d90b0ed64bd38a59d14b741b8bb5f05b0c)), closes [#324](https://codeberg.org/pfarrplaner/pfarrplaner/issues/324)


### Bug Fixes

* Login needs "remember me" ([c693d8c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c693d8c861831258f81d66eba8ae400d64d73857)), closes [#323](https://codeberg.org/pfarrplaner/pfarrplaner/issues/323)

## [2022.154.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.153.0...v2022.154.0) (2022-11-14)


### Features

* New fully AJAX-based LiturgyTree and editors ([ef3b844](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ef3b8442dfdbfaaf776be7d7a7a86c1c754085c8))
* One-click placement of a person in a service in the calendar ([f6c138c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f6c138c77c1843e2347fb7cd73b94321750ace5d)), closes [#316](https://codeberg.org/pfarrplaner/pfarrplaner/issues/316)
* PeopleSelect: Show self first ([2fb6ae4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2fb6ae4c99662876dca4e88a90d661fffd893aab)), closes [#319](https://codeberg.org/pfarrplaner/pfarrplaner/issues/319)
* Support Ctrl+click in calendar ([79e1fd5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/79e1fd5ae02b4a695b9b31292168ef1a0e924434)), closes [#318](https://codeberg.org/pfarrplaner/pfarrplaner/issues/318)


### Bug Fixes

* New person creation in PeopleSelect fails if no spaces are present in raw name ([e32576c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e32576ca4623cafdfd49a79f4fec9748af8b25a8)), closes [#310](https://codeberg.org/pfarrplaner/pfarrplaner/issues/310)

## [2022.153.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.152.1...v2022.153.0) (2022-11-10)


### Features

* Add new report for user AZE statistics ([67b7563](https://codeberg.org/pfarrplaner/pfarrplaner/commits/67b7563ed6cfad50d5e3a623dd16a0824421c89b))
* Add work-time-calculation (AZE) category to services ([272b93b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/272b93b67ca5ed60a0a48eed9273f124be5154fa))
* Create NameService instance from user object ([cca699a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cca699a374bd99bfbf872d3fb6c49ceafc00a4b7))

### [2022.152.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.152.0...v2022.152.1) (2022-11-09)


### Bug Fixes

* locations.alternate_location_id needs to be nullable ([0f9daf9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0f9daf9b0a4ce660fdcb709001df188086e36b76)), closes [#313](https://codeberg.org/pfarrplaner/pfarrplaner/issues/313)
* Pagination problems in multi-page table datasets ([0dd7274](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0dd7274f132bfba87c27fbb9c7623d4f08c4eed7))
* PlanningInput does not sort and paginate correctly ([ba6a31a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ba6a31a7731c544e38a3b82583a1feb2fa5256da))
* WeddingEditor does not save date for dimissorial request ([f6c932a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f6c932a5240aece1a7d6550ec5941c750d679397)), closes [#315](https://codeberg.org/pfarrplaner/pfarrplaner/issues/315)

## [2022.152.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.151.3...v2022.152.0) (2022-10-24)


### Features

* New assistant to add birthday visits to calendar ([1853cf0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1853cf0e599823737ee0b0aa231bb4090689c259)), closes [#309](https://codeberg.org/pfarrplaner/pfarrplaner/issues/309)

### [2022.151.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.151.2...v2022.151.3) (2022-09-30)


### Bug Fixes

* inputs show unsorted list of services ([9be31a9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9be31a9da41fe6fe36bd14f7e6335b89f27b49fc))
* Wrong title in offering plan input ([e3d7296](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e3d729626be4855c37f406f740036701f54e6258))

### [2022.151.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.151.1...v2022.151.2) (2022-09-26)


### Bug Fixes

* Mobile-size single-day view shows current date instead of selected one ([5b43cc8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5b43cc85403687d5ee880566adcb94d7db1ff7bd))

### [2022.151.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.151.0...v2022.151.1) (2022-09-26)


### Bug Fixes

* Missing parenthesis ")" in liturgy import list ([f9ac72e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f9ac72eae9d803a71aac71c4da19dd95eda4d3a9)), closes [#305](https://codeberg.org/pfarrplaner/pfarrplaner/issues/305)
* Service slug uses UTC time ([a23b54e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a23b54e2daf379b7d6e2db8d19e9a4cef8795c51)), closes [#304](https://codeberg.org/pfarrplaner/pfarrplaner/issues/304) [#299](https://codeberg.org/pfarrplaner/pfarrplaner/issues/299)

## [2022.151.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.150.0...v2022.151.0) (2022-08-17)


### Features

* Add absences in diary export ([4f467d9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4f467d9eea9dbe8e98b335a8126871382f64ec93))
* Add page numbering in diary export ([37acfd2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/37acfd2efa5630be30e99dcd9c30ac0f6d2ac839))

## [2022.150.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.149.2...v2022.150.0) (2022-08-17)


### Features

* Advertise new diary functions on landing page ([baf8ffd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/baf8ffd2db7457bb6b9c10da5fbbbab7576a6ac4))
* Allow auto-categorizing calendar entries as DiaryEntries ([586b088](https://codeberg.org/pfarrplaner/pfarrplaner/commits/586b088f22e304f939bba8133875b0d6aa2b09b7))
* Allow creating new DiaryEntries ([1d0fe44](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1d0fe44b9ea0a560b196a870b8d33af3934e6baf))
* Allow editing and deleting diary entries ([57412f5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/57412f5e582bf4f577bfcec91409b15d8ba5ae1b))
* Auto-sort services into official diary ([6d22900](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6d2290043cbe9cfdd4041115979ffef5fedb12dc))
* Basic infrastructure for official diary (services) ([298a171](https://codeberg.org/pfarrplaner/pfarrplaner/commits/298a1710935505a04401110269c852df0bc9f4e1))
* exclude existing calendar entries / service entries from addable events ([d34fb33](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d34fb3328fc1b98dc2adc11df09df652573979c9))
* Import events from Outlook into official diary ([d8d0214](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d8d02140da5815a9de1e2bc2d8afbcd498807a4d))
* UI refinements ([3fa7146](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3fa71460e0ddb4665ed5f68e6696d2cc235df98f))


### Bug Fixes

* Canceling modal window via titlebar button is not distinguishable from cancel button ([8757555](https://codeberg.org/pfarrplaner/pfarrplaner/commits/87575556f474185bcaa2da0ac45cd759cbf6a00f))
* typo ([92f894c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/92f894c0c8631e286e7f974bd9a455dfcd904ab9))

### [2022.149.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.149.1...v2022.149.2) (2022-07-28)


### Bug Fixes

* KonfiAppQRReport did not include newer services ([0a7761f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0a7761fb91447d1f6571a37c08bf89ad713e08e3))

### [2022.149.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.149.0...v2022.149.1) (2022-07-28)


### Features

* Minor tweaks to rites module ([4708f51](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4708f51f63d3b5dd59ce1c01b63a3d7afdded280))

## [2022.149.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.148.0...v2022.149.0) (2022-07-28)


### Features

* New rites module ([dc1ce11](https://codeberg.org/pfarrplaner/pfarrplaner/commits/dc1ce1146cd5e342c1f61bc2669a88d768f303e6))

## [2022.148.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.147.1...v2022.148.0) (2022-07-28)


### Features

* Format names "first last" in service titles with rites ([bd00576](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bd005763e538dd1525c1034cc8d538c447ae163b))


### Bug Fixes

* ICAL does not show newer services ([029a322](https://codeberg.org/pfarrplaner/pfarrplaner/commits/029a322170c83aaf4d3d369e59b1878a5f42e830))

### [2022.147.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.147.0...v2022.147.1) (2022-07-22)


### Bug Fixes

* FuneralWizard does not fill in current user as pastor ([c4f4aa6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c4f4aa64399e45a57094d9164f9b551ddd6dcca7))
* LiturgicItemHelper throws an exception when names are not correctly formatted "lastName, firstName" ([97bb219](https://codeberg.org/pfarrplaner/pfarrplaner/commits/97bb219f580a9d38f707583d7191c5ac7303b922))

## [2022.147.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.146.0...v2022.147.0) (2022-07-07)


### Bug Fixes

* A4LiturgySheet, A5LiturgySheet fail when a song has no songbook abbreviation ([f615f4f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f615f4f4da87d1e6250685efcbe3f0f14a807302))
* ExceptionMail is missing primary context info ([9f6cb66](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9f6cb66856c9c27e5c4074b9fecd2e861b90e3a3))

## [2022.146.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.145.0...v2022.146.0) (2022-07-06)


### Bug Fixes

* Cannot send SongMailLiturgySheet when a psalm in the liturgy has no songbook ([d77e24f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d77e24f88729000d47872a9be6109cd70701023c))

## [2022.145.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.144.0...v2022.145.0) (2022-07-01)


### Features

* Allow setting KonfiApp event type when creating multiple new services ([c2f254d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c2f254db85d5693c7f5109e204386a5d54e4131d)), closes [#285](https://codeberg.org/pfarrplaner/pfarrplaner/issues/285)


### Bug Fixes

* Outputs fail when no pronoun set is selected for a funeral ([8acb29d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8acb29dffac3d3280c41aa7e1d647a6538b02341))

## [2022.144.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.143.3...v2022.144.0) (2022-06-24)


### Features

* Better error page for 500 ([33ff446](https://codeberg.org/pfarrplaner/pfarrplaner/commits/33ff4464c4a4482f19ec5916711bfd7a3ef86824)), closes [#267](https://codeberg.org/pfarrplaner/pfarrplaner/issues/267)
* Better exception handling ([713d59b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/713d59b629a98c19b7e7001692611f23cd7689db))
* Show past week in CC plan ([4b3d8bb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4b3d8bba5564abe20e88fc83007cb12331a9dbdc)), closes [#263](https://codeberg.org/pfarrplaner/pfarrplaner/issues/263)

### [2022.143.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.143.2...v2022.143.3) (2022-06-24)


### Bug Fixes

* Include weddings in EmbedEventsTableReport ([ea59f4d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ea59f4d79dd92751d9ac27b92c0b67066ac6d976))

### [2022.143.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.143.1...v2022.143.2) (2022-06-24)


### Bug Fixes

* Satuday announcements only cover a single day ([49361a3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/49361a383a459724baf36e1a2b626201aae24657))

### [2022.143.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.143.0...v2022.143.1) (2022-06-21)


### Bug Fixes

* Song verses are not sorted numerically ([6b2e87e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6b2e87e710aafe013817f4d47722425f1f76ae05)), closes [#280](https://codeberg.org/pfarrplaner/pfarrplaner/issues/280)

## [2022.143.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.142.5...v2022.143.0) (2022-06-20)


### Features

* Implement A3 song list ([7e40b9a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7e40b9a7f672d03c0e2e55b32a0f69bf0007454e)), closes [#279](https://codeberg.org/pfarrplaner/pfarrplaner/issues/279)


### Bug Fixes

* iCal links stopped working ([394b97e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/394b97ebb2481dde91319718c4f3eea2ceee3af0))

### [2022.142.5](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.142.4...v2022.142.5) (2022-06-03)


### Bug Fixes

* MinistryRequest ignores selection ([948212e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/948212e82cd8380d101b15d32099f185f1aef687)), closes [#275](https://codeberg.org/pfarrplaner/pfarrplaner/issues/275)

### [2022.142.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.142.3...v2022.142.4) (2022-05-20)


### Bug Fixes

* Cannot add verses on new songs ([0fd6769](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0fd67696ac419c177cc70a70b7a16593c14af0ab)), closes [#268](https://codeberg.org/pfarrplaner/pfarrplaner/issues/268)
* Cannot type continuously in funeral appointment place field ([0189b2c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0189b2c4edc369f73b3e55eebe8f2e55432aaccf)), closes [#269](https://codeberg.org/pfarrplaner/pfarrplaner/issues/269)
* Loading message has no spinner ([9af408b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9af408bc9194b1507cace29a4cff4c3d66ca3980))
* Songs without songbooks do not appear in song list ([b774935](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b774935494b4ab16bf3773b2acfcb15c1f4aa632))

### [2022.142.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.142.2...v2022.142.3) (2022-05-12)


### Features

* Optionally include page numbers in FullTextLiturgySheet ([9d31474](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9d31474cc47c0012d61c09531f1221ab30b3aba6)), closes [#265](https://codeberg.org/pfarrplaner/pfarrplaner/issues/265)

### [2022.142.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.142.1...v2022.142.2) (2022-05-06)


### Bug Fixes

* Funeral: Relative date text is one week off ([cfbd7ef](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cfbd7efd98921abfcdaee4adcc3a2ccbc27fdb90)), closes [#264](https://codeberg.org/pfarrplaner/pfarrplaner/issues/264)

### [2022.142.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.142.0...v2022.142.1) (2022-05-06)


### Features

* Show one week back in ChildrensChurchReport ([ea3bcc9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ea3bcc9f720e853bb9dc6a868d15ef6ceaffbf13))


### Bug Fixes

* Funeral: Relative date text is one week off ([962b931](https://codeberg.org/pfarrplaner/pfarrplaner/commits/962b9319ea1bffa8d53dbac6016daff3c0c2ef2d)), closes [#264](https://codeberg.org/pfarrplaner/pfarrplaner/issues/264)

## [2022.142.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.141.2...v2022.142.0) (2022-05-04)


### Features

* Add new songbooks ([15b5807](https://codeberg.org/pfarrplaner/pfarrplaner/commits/15b5807cd479ef4ec3291a406306aa733566eeca))

### [2022.141.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.141.1...v2022.141.2) (2022-05-02)


### Bug Fixes

* PsalmEditor uses unauthenticated API route ([a443efb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a443efb72d15a44ad7e112877da3f46550573e3f))

### [2022.141.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.141.0...v2022.141.1) (2022-05-02)


### Bug Fixes

* Wrong button actions ([122d6cb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/122d6cbaee8e5112fa641d76e7b26bb78b7451b2))

## [2022.141.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.140.0...v2022.141.0) (2022-05-02)


### Features

* Add Psalms admin module ([06cffaf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/06cffaf04847626e521e718279840c08064f5ae6))


### Bug Fixes

* Bots (like Twitter) do not see images for sermons ([3380d29](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3380d29dbfcd679e17873f50e4f6acdeae8c4a31))
* Cannot delete verse in SongEditor ([a06c638](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a06c638d52e5a90575ebce70f16d5649539171e8))
* Songs/Index is very slow ([d69de4e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d69de4e305aaa910fda592d88f392e9ddee41ab0))

## [2022.140.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.139.0...v2022.140.0) (2022-04-24)


### Features

* Show streaming details on StreamingTab ([8979bdf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8979bdfbd74b7b85d7467a95fdedf1429a682817)), closes [#260](https://codeberg.org/pfarrplaner/pfarrplaner/issues/260)

## [2022.139.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.138.1...v2022.139.0) (2022-04-24)


### Features

* Show service title in MinistryRequestReport selector ([3202643](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3202643dee72b18432f1d8a1f32ff1f3ca01a8a9)), closes [#258](https://codeberg.org/pfarrplaner/pfarrplaner/issues/258)


### Bug Fixes

* Broadcast list does not include all broadcasts ([3aa62ce](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3aa62ce85fb259b32c913d1f15c1719f59276f2a))
* Removed monitoring from StreamingTroubleshooter ([41d682d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/41d682d759ef76af4d265d0866fc56ddd06a9153))
* UpdateYoutubeStream command fails when getVideo return null ([7e98b48](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7e98b48822ffa021122c331fc7f9f92d7e1617a0))

### [2022.138.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.138.0...v2022.138.1) (2022-04-21)


### Bug Fixes

* Cannot create new songs from LiturgyEditor ([da4202f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/da4202f884fc2596508be36f5c5cc5500cc880e4))
* Liturgy button in ServiceEditor does not use Inertia ([b1d51ea](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b1d51ea29c7b6f7c30fea3c9adc6f767ae3e8686)), closes [#254](https://codeberg.org/pfarrplaner/pfarrplaner/issues/254)
* Sermon editor uses bold text ([9afcd14](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9afcd14839deef1d252dfef94fa0598090343c69)), closes [#251](https://codeberg.org/pfarrplaner/pfarrplaner/issues/251)

## [2022.138.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.137.3...v2022.138.0) (2022-04-20)


### Features

* Use re-captcha on public contact form ([51bd132](https://codeberg.org/pfarrplaner/pfarrplaner/commits/51bd132f5ddb27bb853f2139efbdd1fdd769a7d9)), closes [#214](https://codeberg.org/pfarrplaner/pfarrplaner/issues/214)


### Bug Fixes

* extranet fails due to obsolete day object ([22c408f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/22c408f10ecdd783a66ed90d7e791c03c25302b5))

### [2022.137.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.137.2...v2022.137.3) (2022-04-14)


### Bug Fixes

* SingleDay view on mobile does show services ([ce265ea](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ce265ea38a861fcb64f1d2cc479cf2cbafbee8d9))

### [2022.137.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.137.1...v2022.137.2) (2022-04-13)


### Bug Fixes

* dataReplacerTitle() fails when a rite has been chosen as replacement and then was deleted ([f665514](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f665514b150a99e75602a555a7db460214fe35b6))

### [2022.137.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.137.0...v2022.137.1) (2022-04-13)


### Bug Fixes

* SongMailLiturgySheet does not include songs ([2f04097](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2f0409791f66d567f1548497b63a2c25489f7698))

## [2022.137.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.136.0...v2022.137.0) (2022-04-11)


### Bug Fixes

* Cannot add new songs ([e3c7f7c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e3c7f7c4121f570635b5e83d6a62d60594920869))

## [2022.136.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.135.0...v2022.136.0) (2022-04-11)


### Features

* Admin interface for songbooks ([4836514](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4836514038dd379c61a7cc8b188b70351c8db104))
* Song admin ui ([e3b7e25](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e3b7e25b323d89163f6aae9cb534c72625d267f5))
* User proper authorization for songbooks admin ui ([096793c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/096793c3ec8d2aa4004a659d4d4e5d11020a309d))


### Bug Fixes

* Return admin modules sorted ([48599b5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/48599b5a4d37b7087fb79d99396d64633a850746))

## [2022.135.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.134.0...v2022.135.0) (2022-04-10)


### Features

* New admin index page ([1012372](https://codeberg.org/pfarrplaner/pfarrplaner/commits/10123724fab996674e8c7b6c41e8701327d8553e))


### Bug Fixes

* LiturgySheets fail when no song is chosen ([4d23f2e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4d23f2e99771a74d81d0156c355083b113c6cf8c))
* Remaining public blade-based views fail because bundle.js is missing ([8e4d507](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8e4d5075f28b219d7a62ac981799e9922c4ce8a4))

## [2022.134.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.133.0...v2022.134.0) (2022-04-09)


### Bug Fixes

* Adding a songbook causes duplicate entries ([dab51f1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/dab51f1c0d1af49a87615ae9bfef14d8258d4964))
* SermonEditor uses bold font for sermon text ([de96547](https://codeberg.org/pfarrplaner/pfarrplaner/commits/de965474c42f60903e790d0aa6dd54d694157f4e))

## [2022.133.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.132.0...v2022.133.0) (2022-04-09)


### Features

* A song can belong to more than one songbook ([c1a32c0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c1a32c037c0aa9980f9b4e5d5dfb09a9f8f21ca4))

## [2022.132.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.131.0...v2022.132.0) (2022-04-08)


### Features

* Provide button to copy funeral life story to sermon text ([48b9e91](https://codeberg.org/pfarrplaner/pfarrplaner/commits/48b9e91ed7cc1169e1dc88676ae5774011f012c4))

## [2022.131.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.130.0...v2022.131.0) (2022-04-07)


### Features

* Add mouseover title for required field labels ([898b744](https://codeberg.org/pfarrplaner/pfarrplaner/commits/898b7440893a65f99f9c429be76ead578653da6d))
* Add required attribute for FormTextarea ([43a88b4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/43a88b42e85ceb2124a83d5d5f76c45018c75788))
* NavButtons can have a href ([f51c1d1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f51c1d1fa6afdfd09df78b012c5e6b39329fe773))
* Remove heavy dependencies to lighten page load ([d525089](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d5250892c25aafa6f1c33eceddb4853a80d8c1ae))


### Bug Fixes

* Cannot create new role ([0bbb29c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0bbb29c488cfccfe13d77a936214fbf6f8fbf3a4))
* New parishes cannot be created ([b827887](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b827887351f198aadf5dbd19063c6a24dd0f8bdb))

## [2022.130.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.129.1...v2022.130.0) (2022-04-07)


### Features

* Optionally show CC details in calendar ([516887a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/516887a56e5bd38166654a32949201bcbb8f3eaa))

### [2022.129.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.129.0...v2022.129.1) (2022-04-07)


### Bug Fixes

* ServiceTableReport is missing file prefix ([2f68de4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2f68de437f3753514ec193ba985419cf956fffce))

## [2022.129.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.128.2...v2022.129.0) (2022-04-07)


### Features

* Use AJAX for broadcast creation from StreamingTab ([8f58bb9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8f58bb969cd5da06939e992e740dac88df3dc722)), closes [#237](https://codeberg.org/pfarrplaner/pfarrplaner/issues/237)


### Bug Fixes

* Participants counter wrong ([d665455](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d6654551143b8baadef5b6bd8345cdaf88b6c360))
* PredicantReport includes funerals ([b5f7db7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b5f7db737f1d5337eccd2e8146f5b157f0dc402b))
* Tabs sometimes not shown ([c0b6eac](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c0b6eace4701d5a02cedc257366ea22941e2007c))

### [2022.128.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.128.1...v2022.128.2) (2022-04-06)


### Bug Fixes

* Cannot save changes to wedding records when appointment date is set ([d94bd33](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d94bd333eafa9a4d79e13d9d43fa798c7ed8c410))
* Funeral wizard results in wrong service time (timezone problem) ([8ab5610](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8ab561044eaa9572a910d988478c8e2984b1bdef))

### [2022.128.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.128.0...v2022.128.1) (2022-04-01)


### Bug Fixes

* MinistryRequestReport fails to send messages ([ad5d819](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ad5d8195bcc6403fcc4f3c0e3e9995938800ec8c))

## [2022.128.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.127.4...v2022.128.0) (2022-03-26)


### Bug Fixes

* DatePicker uses wrong timezone when iso-date is set ([c224a5b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c224a5b8841f77aefae7a6c6dbbd7c93eac1f20a))
* FormDatePicker applies still applies wrong timezone conversion when handling dates with time ([1285b17](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1285b17ac64f301bfefe5b16aa4142e1a185729a))
* NewsletterReport omits final day in date range ([15e6d2d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/15e6d2d0dedeba678b32d67146cc26504382d0fd))
* PsalmItemHelper throws error when no psalm has been chosen ([f940a92](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f940a926d8871b56426f7869ff04edd6b7158bd9))

### [2022.127.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.127.3...v2022.127.4) (2022-03-21)


### Bug Fixes

* Baptisms/Funerals/Weddings throw a 404 when opened from HomeScreen ([fe2f892](https://codeberg.org/pfarrplaner/pfarrplaner/commits/fe2f892cd32ee8e1399c12dea9c2c6e31396aa7a))

### [2022.127.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.127.2...v2022.127.3) (2022-03-20)


### Bug Fixes

* qr code route not found ([7a53050](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7a53050b7d47f306d9f42691840bee30f36e73fe))

### [2022.127.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.127.1...v2022.127.2) (2022-03-20)


### Bug Fixes

* Streaming troubleshooter fails with invalid sql query ([f4fac0c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f4fac0cf76dc9455d0cf48aa1d5742f976228240))
* Typo ([beef278](https://codeberg.org/pfarrplaner/pfarrplaner/commits/beef2783a23bd6ec37e7b2fc92ab89ec867a26cf))
* youtube:update command still used days in sql query ([f0f1c84](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f0f1c84868e106b6f0037dcf6ac5c2af9fe0c50b))

### [2022.127.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.127.0...v2022.127.1) (2022-03-16)


### Features

* Disable obsolete RegulatoryReport ([9d2563b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9d2563b9071738e7c74ffaacef8b12ba2c6bcdc2))
* Even more tricks to prevent login form token expiration ([fd255c7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/fd255c75e4d1f47b51788a498d290f345d8d6c20))
* Permit changing text of a SaveButton ([28aa47d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/28aa47d06cfb4c6b412f0dc22f5f4c04c7824a06))


### Bug Fixes

* Cannot create new service from calendar ([3d0bef6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3d0bef6b35710972f00f9e8b7193cd3b8c595f97))

## [2022.127.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.126.1...v2022.127.0) (2022-03-14)


### Features

* New index view for reports ([7a7eb70](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7a7eb70e2caddf251f1826c863aefd50b43e7dd2))

### [2022.126.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.126.0...v2022.126.1) (2022-03-14)


### Bug Fixes

* Revert premature changes to SongPPTLiturgySheet ([4a92014](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4a92014aa4a9fafd949d69043acc501a2c9a45d9))

## [2022.126.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.125.0...v2022.126.0) (2022-03-12)


### Features

* group admin urls under /admin ([af6025f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/af6025f52eabc92e533ca61698f691692cfde756))
* Rewrote Role admin in vue ([2812c58](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2812c587223d1b19dfdc1371bbe3409f844ed29d))
* Rewrote Tag admin in vue ([0aa3d17](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0aa3d1729a02018465ae678e64ecc61b718690e8))
* Stabilization for quick layout switches ([0f1fd25](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0f1fd25bc60034a90dc30aa9da8e40a3cf4ffeca))


### Bug Fixes

* Login page CSRF token expires when page remains open for a long time ([75d041d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/75d041d521b6b31a051215a8bae6045626c7bead))
* Missing component imports ([c9ac87d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c9ac87d59ec463bdb3d3c4959bc47b59f3f6ece2))
* Wedding registration document todo item is not checked after upload ([a99f461](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a99f461f22004bb798fed0b6c0f15cf425522c9d))
* wrong method name ([398c8c5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/398c8c51025ab841ee7bcc291ac69d41b914e8b0))

## [2022.125.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.124.0...v2022.125.0) (2022-03-12)


### Features

* Link to wedding service from wedding record ([7cfdfad](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7cfdfad2efd0fb7e5c717b774e9843bae8a3984d)), closes [#233](https://codeberg.org/pfarrplaner/pfarrplaner/issues/233)
* Link to wedding service from wedding record ([e7aa1b7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e7aa1b7fbfb5dab9999452491977690b9e78bab6)), closes [#233](https://codeberg.org/pfarrplaner/pfarrplaner/issues/233)
* Rewrite service calendaring, remove Day objects ([aba9a18](https://codeberg.org/pfarrplaner/pfarrplaner/commits/aba9a18d1cee56006e763a730904376d8e4bb09f))


### Bug Fixes

* Wedding appointment calendar item in Outlook has funeral details link ([3df6521](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3df65218e06ef89cf5c29c819ed26b91dd970073)), closes [#232](https://codeberg.org/pfarrplaner/pfarrplaner/issues/232)
* Wedding appointment calendar item in Outlook has funeral details link ([78d7fb9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/78d7fb9c7b4c9d15cc5dec85baf6d396cd37b241)), closes [#232](https://codeberg.org/pfarrplaner/pfarrplaner/issues/232)

## [2022.124.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.123.0...v2022.124.0) (2022-03-10)


### Features

* Rewritten parish admin ([94affdb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/94affdb905c7cdd3d64262ce130b08bccc30e9b5))

## [2022.123.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.122.0...v2022.123.0) (2022-03-10)


### Features

* Rewritten location admin ([e965926](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e96592675c4ef82e08120faa26b823c1c314f20a))

## [2022.122.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.121.0...v2022.122.0) (2022-03-07)


### Features

* Insert source text into Pixabay images ([26a8b68](https://codeberg.org/pfarrplaner/pfarrplaner/commits/26a8b68fc302bf6cbfd48bbf3badc8a633a101bc))
* Let the user crop uploaded images ([862fff1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/862fff116c13904d6b179be7d1d4e008a56f8d8f))
* Pixabay import and cropping for image attachers ([f423af6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f423af631adc34d33e6a9f03337d179f96c93b1f))


### Bug Fixes

* Line breaks cause invalid .ics file ([6b33d96](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6b33d966c24e17f1e5ec3b4a87adf4ef130472d4))
* Pass api_token to vue ([065eb6f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/065eb6fcf6f5d2e206e53c56f4770b67bc022403))

## [2022.121.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.120.0...v2022.121.0) (2022-03-04)


### Features

* Allow image upload via URL or camera ([cfac44e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cfac44ec644ff2c6580e30e2c4e98f50a20c1037)), closes [#224](https://codeberg.org/pfarrplaner/pfarrplaner/issues/224) [#225](https://codeberg.org/pfarrplaner/pfarrplaner/issues/225)


### Bug Fixes

* city permission not saved on update ([8cc0ec1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8cc0ec1247239b5407eb126c4f5a8640f97bce0c))
* CityEventsCalendarLink includes old events and funerals ([b3a0777](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b3a077742cabf36ca4c46750072cf3b2edb253f7))
* using trueDate() throws errors when time is not set ([8ae5ffb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8ae5ffb48e7a9c6adb5db6246c773fc5c9091a31))

## [2022.120.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.119.5...v2022.120.0) (2022-03-03)


### Features

* Allow local storage of FuneralEditor data ([8dda0f0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8dda0f0ee9bb302d44be0c0a41a119c72826d913))


### Bug Fixes

* After page reload, calculated fields do not show ([fcd86f3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/fcd86f3302b1c11932f3e14751475616e0832851))
* Minor tweaks for FormBibleReferenceInput ([6e485af](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6e485af990fb9729ff341cdc1f04949576acf399))

### [2022.119.5](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.119.4...v2022.119.5) (2022-03-03)


### Bug Fixes

* FormBibleReferenceInput does not always allow input ([acded66](https://codeberg.org/pfarrplaner/pfarrplaner/commits/acded660f2bc83dcafe702fe70f6316324d692d4))
* Login page displays exception when YouTube feed fails ([f6d8f76](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f6d8f761147cd37404d0170459e5167679337079))
* Problems saving ministry request responses ([2cf5b8f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2cf5b8f8cacd95b73d76830b2afefe09ba6eb77d))

### [2022.119.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.119.3...v2022.119.4) (2022-02-25)


### Bug Fixes

* dod_spouse field has formatting errors ([f54f156](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f54f156e4e98fa1bdad18d1db1d1ec8d29490f6a)), closes [#221](https://codeberg.org/pfarrplaner/pfarrplaner/issues/221)
* Enlarge lower border on participants slide ([aac07c1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/aac07c17beb9bf8fe46c28a39e01f7c18615dd58)), closes [#218](https://codeberg.org/pfarrplaner/pfarrplaner/issues/218)
* FuneralEditor uses sermon.text instead of sermon.reference ([e147f62](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e147f629e020e0f5be8054934180fa6cdcde5052)), closes [#222](https://codeberg.org/pfarrplaner/pfarrplaner/issues/222)
* Plan entries disappear one day too early ([a1bafad](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a1bafadb57b158bd8a74c857a040250792f7cd8f)), closes [#219](https://codeberg.org/pfarrplaner/pfarrplaner/issues/219)

### [2022.119.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.119.2...v2022.119.3) (2022-02-22)


### Bug Fixes

* Duplicate field name in LeaveRequestForm ([4ace79e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4ace79e83a4822afae2a073696ea9cdaf0d1f310))
* MinistryRequest response fails because $user is already resolved to a model ([e4bee6c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e4bee6c1c54789c0341b4fec3f1cbed6843ff904))

### [2022.119.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.119.1...v2022.119.2) (2022-02-19)


### Bug Fixes

* Blank page after saving sermon ([dd67900](https://codeberg.org/pfarrplaner/pfarrplaner/commits/dd679000091bc32686eca04e8e8d40e1f930fdbf))
* service qr code fails in FullTextLiturgySheet ([9b1a1a2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9b1a1a2e66e8c9cc7e18bd7bd365ab9d13b2720e)), closes [#213](https://codeberg.org/pfarrplaner/pfarrplaner/issues/213)

### [2022.119.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.119.0...v2022.119.1) (2022-02-19)


### Bug Fixes

* Cannot delete services when relatedCities are set ([6771ef1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6771ef1e61c3053d62be248119a913f06f5ee973))

## [2022.119.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.118.0...v2022.119.0) (2022-02-18)


### Features

* Change background when hovering over item row in LiturgyTree ([0689d1a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0689d1abc111b83d5f84c29f4081ca588a91b0c9)), closes [#203](https://codeberg.org/pfarrplaner/pfarrplaner/issues/203)
* Show unprocessed baptisms/funerals/weddings even when they are older than two weeks ([52fede5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/52fede56ebb92dfc00b5bcc55bdc7d5ecbbcd36f))
* TeamIndex has new admin list view ([a3dd48e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a3dd48e6c42b2881a13e1abfaea736a7418295da))


### Bug Fixes

* Unwanted time shift when sync'ing prep appointments with Exchange calendars ([da7b622](https://codeberg.org/pfarrplaner/pfarrplaner/commits/da7b622ce73caf53c52f8da33780f162f8568994))

## [2022.118.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.117.1...v2022.118.0) (2022-02-18)


### Features

* Use FormBibleReferenceInput for all appropriate inputs ([9d31b0f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9d31b0fdc4287e8b941ecaa58f81cdba5a3fb8f4))


### Bug Fixes

* handle empty reference gracefully ([76c8230](https://codeberg.org/pfarrplaner/pfarrplaner/commits/76c8230a422ddb3d40b2cee0dec397ea0a987b37))

### [2022.117.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.117.0...v2022.117.1) (2022-02-18)


### Bug Fixes

* first contacts not filled in automatically in Baptisms ([ff86677](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ff866779a99d60527d69e00d1bee0831e5fce77f))
* New menu icons not shown in Blade-rendered views ([506f68e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/506f68e2e6a748539c1687143ecabe2a99b6730f))

## [2022.117.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.116.4...v2022.117.0) (2022-02-17)


### Features

* Allow showing service in other cities as well ([bdb9dee](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bdb9deed766be751b0dad41363ce02dcf4b08bc8)), closes [#207](https://codeberg.org/pfarrplaner/pfarrplaner/issues/207)
* Show liturgical day in embedded view ([e25c948](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e25c9483a581dfda1959ccfe882c126c6b78255b)), closes [#210](https://codeberg.org/pfarrplaner/pfarrplaner/issues/210)


### Bug Fixes

* increase MDI icon font size ([5f8c993](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5f8c99343fb940aa33eaac697ab5a7c94d75dab2))

### [2022.116.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.116.3...v2022.116.4) (2022-02-17)

### [2022.116.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.116.2...v2022.116.3) (2022-02-17)

### [2022.116.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.116.1...v2022.116.2) (2022-02-17)


### Features

* Accordion layout in FuneralEditor ([8ad593f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8ad593f3f0f52c50b324cd83bf384f1181d17b4d))

### [2022.116.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.116.0...v2022.116.1) (2022-02-16)


### Bug Fixes

* AbsencePolicy checks User->cities instead of User->homeCities ([6b55504](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6b555040a1a5c9feaa35fb27e0b900953aef91aa))
* FuneralWizard still fails on certain conditions ([64cc11a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/64cc11a7c29992c73ab77a85b1c058014cb7b832))

## [2022.116.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.115.0...v2022.116.0) (2022-02-16)


### Features

* Save confirmation and wedding texts with Funeral ([b6b1342](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b6b13422b996842d9cac27e72b6d0923272b2143))

## [2022.115.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.114.0...v2022.115.0) (2022-02-11)


### Features

* Even more fields and age calculations for FuneralEditor ([d7cf263](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d7cf263101ed025a569df43530afdeb9a195ec77))


### Bug Fixes

* Email field only accepts one character at a time ([08060b1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/08060b142d5ff068b77ca99491c8563f106b844e))

## [2022.114.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.113.1...v2022.114.0) (2022-02-11)


### Features

* Add birth-related fields to Baptism ([9c27877](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9c27877a5fe92fae44129748d8e216dba000d965))
* Add no-padding in AdminLayout ([98856c1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/98856c1a9b128e7426f6324ce418c5ee4b09dff7))
* New artisan upgrade command to facilitate new version installs ([4965c81](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4965c81fb29cee15cd1c516340b14a45ea5808d1))
* New, cleaner, card-less layout in Vue-based forms ([ea9e50b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ea9e50b9efcfa1c74b0e14934180d7c16aa89738))


### Bug Fixes

* InfoPane is not fluid ([884adc6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/884adc6e3aa1de6cb2fcb663c3efc1ddf68d1fca)), closes [#202](https://codeberg.org/pfarrplaner/pfarrplaner/issues/202)
* Only show fake attachments for self-editors ([529ace0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/529ace059b2a7a7a08dc09f9f6eff6ce7a2f020c))
* Prevent empty attachment list message ([750dd74](https://codeberg.org/pfarrplaner/pfarrplaner/commits/750dd7420c98b9ee7103974abafa55f2bd7ffaa1))
* Sort cities by name ([d1984c8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d1984c85bdf51025a3c230108e4502871ae33e6e))
* typo ([82a9c2f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/82a9c2f0e64915727a55957c5e03a1ab2b0669bf))

### [2022.113.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.113.0...v2022.113.1) (2022-02-10)


### Bug Fixes

* All users are self-editors in AbsenceEditor ([64bcd58](https://codeberg.org/pfarrplaner/pfarrplaner/commits/64bcd581bf4355f02f04d56b7f41189fa8abf69e))

## [2022.113.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.112.2...v2022.113.0) (2022-02-10)


### Features

* allow FakeAttachment with custom download action ([a8738c0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a8738c0328713bc243cd4c7b3925cbd6cd010a75))
* allow suppressing empty list message in AttachmentList ([57d365a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/57d365ac8d1ebbd4d4da130d0937d181fcffc7d9))
* file attachments for absences ([ed4f2e6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ed4f2e690e1af36a50d682f807a9f877fed72d03))
* New card-less base layout ([6806a27](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6806a27911093e3df31e640351644ff00d3c9383))

### [2022.112.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.112.1...v2022.112.2) (2022-02-10)


### Bug Fixes

* Admins who are not pastors see "eye" icons, even when they cannot hide users ([6c66306](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6c6630669a8eaa756c0e2b5bb33ff119c77e4147))

### [2022.112.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.112.0...v2022.112.1) (2022-02-10)


### Bug Fixes

* Admins who are not pastors only see their own absences in Planner ([cc7008b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cc7008b8b3ddfbc3fe8c268ee0b40e6540c98bfb))

## [2022.112.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.111.2...v2022.112.0) (2022-02-10)


### Features

* Group users in absences.index ([b30c520](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b30c520a828c5f612ccc023d719294a43e837890)), closes [#199](https://codeberg.org/pfarrplaner/pfarrplaner/issues/199)


### Bug Fixes

* Deleting a user throws Exception ([61de159](https://codeberg.org/pfarrplaner/pfarrplaner/commits/61de159c07b341792e67141e6ea321f582147247)), closes [#200](https://codeberg.org/pfarrplaner/pfarrplaner/issues/200)
* LeaveRequestFormReport/TravelRequestFormReport have wrong ->to date ([6293368](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6293368f5e5b9a4eb5b058cdb1666a666e973aa5)), closes [#201](https://codeberg.org/pfarrplaner/pfarrplaner/issues/201)

### [2022.111.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.111.1...v2022.111.2) (2022-02-08)


### Bug Fixes

* Duplicate service slugs ([e3f8f8b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e3f8f8b80c269adbc20a51b9637b3f09c879bb69))

### [2022.111.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.111.0...v2022.111.1) (2022-02-08)


### Bug Fixes

* Errors produced by empty records ([905d9a9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/905d9a9cb1b12f3d2af93072bb4069aa3d65344f))
* Missing permissions for UserPolicy::index ([349ba4f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/349ba4f7863e28dcf2f4ed7f5193d138cd6c8463))

## [2022.111.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v2022.110.1...v2022.111.0) (2022-02-08)


### Features

* Configure alternate home route in UserEditor ([d348502](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d348502224a596c6198c28de45c25e11d2408bb6))


### Bug Fixes

* Old Blade-rendered views show profile menu item twice ([5139fac](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5139facc8d8f68d8467cdc8bf9333bb1edd31265))
* Users with alternate home route are not forced to change initial password ([f4b6348](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f4b6348f2580ae4bd3923c410f0c529fbeda0065))

### [2022.110.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.110.0...v2022.110.1) (2022-02-08)


### Bug Fixes

* New version not reflected in package.json ([63981c9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/63981c93ec2eaacdc5cc27fa21cfbf86510e30f2))

## [1.110.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.109.0...v1.110.0) (2022-02-08)


### Features

* Adopt new year-based versioning schema ([06de6d4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/06de6d4d02cc975b5e9ef81b4648f1f9c433a66f))

## [1.109.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.108.0...v1.109.0) (2022-02-08)


### Features

* Configure a user's menu items in UserEditor ([510b6b6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/510b6b681254883d95badda1f778340941ffc0b0))
* Permit skipping HomeScreen entirely ([383c568](https://codeberg.org/pfarrplaner/pfarrplaner/commits/383c568513302ae40bbc20518628f0403a33e752))


### Bug Fixes

* landing-page logins often fail due to expired tokens ([59d7ccf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/59d7ccf1a7f74cb4f4e13787dc63dcd082323790))

## [1.108.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.107.0...v1.108.0) (2022-02-07)


### Features

* Adapt mailing titles for absence notifications ([05e4615](https://codeberg.org/pfarrplaner/pfarrplaner/commits/05e46151a4c969d7bb88d9f4396fb9859817de79))
* Create new user accounts with random password ([d8fdeba](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d8fdebade31eb1b93c82d21f42c669db67583b85))
* More functionality for new user administration ([0c31aea](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0c31aea6499c24fc2f3f7ac6307e15475984c94b))
* More functionality for new user administration ([4beaa78](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4beaa78e13bce79df90418c611940bf6a1ef882a))
* New index view for user admin ([5f475c2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5f475c2df1b30ae111a5110f1c604e46cc16e9a1))
* New UserEditor ([9a0e6fc](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9a0e6fcf1828223d62912d4e421d3359765214c0))


### Bug Fixes

* AdminTab uses non-Inertia link to UserEditor ([1dd3847](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1dd38478d0d30fe794f5d4b3c893780c018b1e63))
* Inertia does not send flash messages ([fe4219b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/fe4219bfa61ff985c6930c38196cd3f22c61cd7b))
* User passwords hashed twice ([89681f2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/89681f24742b5b8a6dab92c759094407e9a39f9d))

## [1.107.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.106.0...v1.107.0) (2022-02-04)


### Features

* Better nav buttons in AbsenceEditor ([932d7e2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/932d7e2a7bf55f74238ed83c2a0e6e5edc46042f))
* Better planner layout ([e963011](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e96301135a756fe67a74c74779bd3d7d6ac56737))
* Display correct version string in Blade-rendered views ([e713684](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e71368457bc3567827f0291c16e4bbbd713f21ff))
* More improvements to DemoBuilder ([f5acf0c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f5acf0c06ba150d184ed31fbfdbea9158bdbdb81))
* PackageService returns additional info ([48fba44](https://codeberg.org/pfarrplaner/pfarrplaner/commits/48fba44a145b57f0776bf8f2c110b6d94951a573))
* Show absence details only to qualified users ([492efb0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/492efb056ba77172f4e4b5d153f19ccf42ea9846))
* Show new landing page for demo ([ff853d9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ff853d9124bf1f117960894d8cf79b10a5beffa2))


### Bug Fixes

* AbsenceController tries to send mail to users without email address ([78a5acc](https://codeberg.org/pfarrplaner/pfarrplaner/commits/78a5acc2aebf96bec46fe5939eb218ef60bbf057))
* DemoBuilder produces English faker data ([cbc5ad2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cbc5ad267d71aca825ccd5a0b1dade30ec9c1f21))
* Minor layout fix to demo login ([016f48c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/016f48c1bd097c2dc2ced604bda264279b696259))
* Multiple fixes in DemoBuilder ([5bcfaed](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5bcfaede60b0c0189924e2bbf6e5409f89ee7d07))

## [1.106.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.105.0...v1.106.0) (2022-02-03)


### Features

* Add AbsenceRequestsTab to HomeScreenTabs ([6ec98dc](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6ec98dc6d936db38f9d80df66f429537788a43d1))
* Further configuration for managing absences for a user ([52b6839](https://codeberg.org/pfarrplaner/pfarrplaner/commits/52b683958b012fd4413545ef5abf152b4d0e3256))
* New system for checking/approving absences ([aad5799](https://codeberg.org/pfarrplaner/pfarrplaner/commits/aad57998b253889839e191a155ea65e0be1246dd))


### Bug Fixes

* AbsenceController uses obsolete route name ([3ca2898](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3ca2898f802ca86805de80279c7168633b6c3c82))
* AbsenceRequest.php uses Auth facade to get current user ([4dfc052](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4dfc0528bc8f1e398e781447369ab5c3fd1f319a))
* Better redirect when absence is rejected/deleted ([02f2025](https://codeberg.org/pfarrplaner/pfarrplaner/commits/02f2025406d64d6161f7816090792f874b600daa))
* DateRangeInput has wrong start date and cannot be disabled ([af40d40](https://codeberg.org/pfarrplaner/pfarrplaner/commits/af40d40ce7b887dbaa910542c4d95086fe7f6beb))
* duplicate route name ([cd35252](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cd352525b68dd7bbd854c1107171b23ab3acd30f))
* Edge recognizes site language as English ([b7e47dd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b7e47dd6cc48dbf947d47309df5eb286479f6cd5))
* FuneralWizard fails with Exception ([705ba25](https://codeberg.org/pfarrplaner/pfarrplaner/commits/705ba257fa8064f81e5d716f1f84861eb0c15dee))
* Obsolete absences.* route ([7b66852](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7b66852c6c8581b99db3cb4439bcf6d261e3a503))
* Obsolete absences.* route names ([13970ce](https://codeberg.org/pfarrplaner/pfarrplaner/commits/13970ceacec78801ff67bb0a3242f391daade072))
* PeopleSelect cannot be disabled ([23c8def](https://codeberg.org/pfarrplaner/pfarrplaner/commits/23c8deffd40c4dfe4d0720d77f11219dda7014b6))

## [1.105.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.104.0...v1.105.0) (2022-01-31)


### Features

* Generate QR code images on-the-fly ([ae84b81](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ae84b81fd6dd9cdd85a49d4de07194cea5bdfa6e))
* Include QR code in FullText ([9e0df99](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9e0df99e7bcb99c58cab489363c796dedb77661f)), closes [#197](https://codeberg.org/pfarrplaner/pfarrplaner/issues/197)


### Bug Fixes

* PPTSongSheet configuration overflows screen size on some screens ([109c76e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/109c76e3106bb84c6bb016ae67e510fbcd460913))
* Raise storage limit for backups ([a9925fd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a9925fda5a1ce53c839b64341c004a785528c280))
* Update mail is sent even when there are no changes to report ([822fe13](https://codeberg.org/pfarrplaner/pfarrplaner/commits/822fe13dd8bc318bcd1ccb264879ba9c32562174))

## [1.104.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.103.3...v1.104.0) (2022-01-27)


### Features

* Grant appropriate permissions when replacing another person ([9815752](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9815752d3d33468d18d269e95a1b6e38203d8ae3)), closes [#196](https://codeberg.org/pfarrplaner/pfarrplaner/issues/196)

### [1.103.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.103.1...v1.103.3) (2022-01-26)


### Bug Fixes

* alt_liturgy_date sometimes has wrong format in requests ([1e9ffb6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1e9ffb6ca72e20772133176877adfbeab8490f6c))

### [1.103.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.103.1...v1.103.2) (2022-01-26)

### [1.103.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.103.0...v1.103.1) (2022-01-26)


### Bug Fixes

* DateRangeInput refuses certain dates ([6f0e072](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6f0e072ec3af34b3ef3562e24b775c479ebd80d9))

## [1.103.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.102.0...v1.103.0) (2022-01-22)


### Features

* Allow configuring SongSheet ([aca6421](https://codeberg.org/pfarrplaner/pfarrplaner/commits/aca642185fab9e4752ace8eb1c82a664b29c7068))

## [1.102.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.101.1...v1.102.0) (2022-01-22)


### Features

* Allow showing liturgy information for another day ([7492f07](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7492f07559faa4749b0e798bd7dfa3ea1771a82a)), closes [#194](https://codeberg.org/pfarrplaner/pfarrplaner/issues/194)

### [1.101.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.101.0...v1.101.1) (2022-01-21)


### Bug Fixes

* Manual does not show images with filenames other than img* ([bef8765](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bef8765225a1dcbe3584e7ac67b32ec8c0fb9990))
* Minor layout tweaks ([2b0deb9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2b0deb9d008a667447ef7e46f53f85419f349775))

## [1.101.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.100.0...v1.101.0) (2022-01-21)


### Features

* Open MusicEditor in new window ([976e92f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/976e92fb78f8ddc47f6e1fa5c45caa22b4970bf7))


### Bug Fixes

* DateRangeInput sets incorrect range limits ([822d4dd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/822d4ddd412baf9781ff1a3a2a3ca30cae3b7222))
* Only show music icon when music is actually present ([92a4f77](https://codeberg.org/pfarrplaner/pfarrplaner/commits/92a4f77c11272caafee6395a99e342b5f2fb8225))
* PPT fails when a song does not have music ([3d9dcba](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3d9dcbabb62cbbd624b88b52eb988c5226e7ab54))

## [1.100.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.99.0...v1.100.0) (2022-01-19)


### Features

* Add music to SongPPTLiturgySheet ([4e99825](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4e99825f121fe48901a1f9434d366727e342caac))
* Add refrain to music rendering ([5112f34](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5112f34697979a04c8ce22f8880d37a5adbebf91))
* Add refrain to MusicEditor ([42a0554](https://codeberg.org/pfarrplaner/pfarrplaner/commits/42a05546db337f7e579fa565cda6a9604c93521b))
* Implement basic infrastructure for musical notation in songs ([15ea603](https://codeberg.org/pfarrplaner/pfarrplaner/commits/15ea6030238ce91aff31045962da859b269b7f5e))

## [1.99.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.98.0...v1.99.0) (2022-01-14)


### Features

* Add duplicates wizard ([d508cc1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d508cc18e0bb3d85c760918bba34c98a778e3eb6))
* Add external content fields to user record ([c500c5f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c500c5fe8ab59b417ccc0a853cd2012a54d2e460))
* Add logo and official name to City records ([ce0cc47](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ce0cc47db1abd6a9ab46399190156a0ff42a205e))
* Show DOD, place of death, age over "Lebensende" field in FuneralEditor ([35b8204](https://codeberg.org/pfarrplaner/pfarrplaner/commits/35b82042956e36e1b820da712ede754b15e53c3a)), closes [#191](https://codeberg.org/pfarrplaner/pfarrplaner/issues/191)
* Some tweaks to backup table on AdminTab ([4cbf4b1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4cbf4b19c5ce5331ef4a508fb87bd98bd48289f9))


### Bug Fixes

* incomplete attributes ([535f8a3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/535f8a3157af578b99486283f32388c804eb868e))
* Landing page has wrong favicon ([fb2fcc7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/fb2fcc7f66758d4870ebd5329b192d48af835871)), closes [#192](https://codeberg.org/pfarrplaner/pfarrplaner/issues/192)
* Return empty result set when external calendar is not configured ([3d6ff45](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3d6ff4577193b786d9198f0ac8873159c86eb62c))
* User cannot use own email address twice ([0f93645](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0f93645d01ae7477dc0d41f8c9a7f28eebccee86))
* Users can have duplicate email addresses ([d89ba85](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d89ba85475665398a846e715c1ac2b6efbdcdd31))

## [1.98.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.97.0...v1.98.0) (2021-12-30)


### Features

* Create basic AdminTab with user quick-select ([232c94f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/232c94fb1b50005b01a97eb09daa7b8df18c0ddf))
* Show backup status in AdminTab ([63a40ea](https://codeberg.org/pfarrplaner/pfarrplaner/commits/63a40ea676d55802c6f89a2b480dc1bed1d63554))

## [1.97.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.96.0...v1.97.0) (2021-12-29)


### Features

* Create a public landing page ([a94bb1c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a94bb1c0dcc113571f71c3bee49b6b10021f9503))
* Implement public-facing contact form ([6731e6b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6731e6b8467f679b9c7caa30685429e1a593788c))

## [1.96.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.95.0...v1.96.0) (2021-12-28)


### Features

* Allow choosing another pastor in FuneralWizard ([bf925f2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bf925f2643a88e2029922ab572611596cdc70e07)), closes [#179](https://codeberg.org/pfarrplaner/pfarrplaner/issues/179)
* Include videos from Youtube channel on welcome page ([a5846e7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a5846e783ad063473ad7421110d139365ef5978b)), closes [#156](https://codeberg.org/pfarrplaner/pfarrplaner/issues/156)


### Bug Fixes

* Image attachments not filtered correctly ([206b214](https://codeberg.org/pfarrplaner/pfarrplaner/commits/206b21470811d21729a20cf8e292dfd9ae780fca))

## [1.95.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.94.0...v1.95.0) (2021-12-27)


### Features

* Add MinistrySignupSheetReport ([4e3f6bc](https://codeberg.org/pfarrplaner/pfarrplaner/commits/4e3f6bc8dfb007cea9e389c949e0db5afeeca2b8))


### Bug Fixes

* PeopleTab has wrong participant count ([b0e2021](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b0e2021ec3c69b5834a60af6372f684a14e2f48b))

## [1.94.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.93.4...v1.94.0) (2021-12-27)


### Features

* Improved service titling ([a5d82ce](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a5d82ce1ba260d25996b7fdab52874c565971663))
* Show attached images on interview tab in FuneralEditor ([6920e2b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6920e2ba1d2a696f72f7621a84a7f97f32bb6e07))


### Bug Fixes

* Service update deletes all existing participants when request has no participants data ([96e3ca1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/96e3ca112dcc87b467f451cc967115fde763ae58)), closes [#177](https://codeberg.org/pfarrplaner/pfarrplaner/issues/177)
* Text/reference fields only accept one character at a time ([6260f28](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6260f2836b77e4d6f2840aaf9be084756df3eee4))

### [1.93.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.93.3...v1.93.4) (2021-12-25)


### Bug Fixes

* Adding an existing sermon to a services wipes all ministry assignments ([8330cbe](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8330cbe39a2c1c689bf52676f8812a4bfaf63b52)), closes [#177](https://codeberg.org/pfarrplaner/pfarrplaner/issues/177)

### [1.93.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.93.2...v1.93.3) (2021-12-21)


### Bug Fixes

* User profile data cannot be updated ([41365b9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/41365b978f9f6c3ff273bb6ecfbd1d1d807ab424))

### [1.93.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.93.1...v1.93.2) (2021-12-21)

### [1.93.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.93.0...v1.93.1) (2021-12-21)


### Bug Fixes

* Menu sidebar scrolls out of view ([6a319b1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6a319b11ed9e0c6bdacde1c8178d2bc66bf93755))

## [1.93.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.92.1...v1.93.0) (2021-12-21)


### Features

* Shortcut buttons for sermon reference in SermonEditor, FuneralEditor ([8748dfb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8748dfb31949001040492778cdc0381864eeabd3))

### [1.92.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.92.0...v1.92.1) (2021-12-20)


### Features

* Allow overriding liturgical day names in config ([ee72d25](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ee72d25769bdc2d2ffe369c139f95176bb5a932c))

## [1.92.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.91.0...v1.92.0) (2021-12-20)


### Bug Fixes

* corrected §§ numbers for CoronaVO ([5ecabd9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5ecabd93efb19fd4da606664acbc7664d11fb4e3))
* Remove text field over signature area in LeaveRequestForm ([b359668](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b35966826f7c03962baf400ffa69db914e828662))
* Temporarily remove day title from embedded registration form ([0fd8313](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0fd8313bb567ea4fa27c89c678a07719d5a626ab))

## [1.91.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.90.1...v1.91.0) (2021-12-16)


### Features

* Highlight unspecified item time ([f1ab6bd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f1ab6bd746a46c256c012b31de4d2c1375da2838))
* Show total service length in LiturgyTree ([b009756](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b009756e45a2084c45af2fbdb6a8704b68429970))


### Bug Fixes

* Speech time estimates fail when a song is not set ([aff6e51](https://codeberg.org/pfarrplaner/pfarrplaner/commits/aff6e5100c64ed4dd9b3b3f3170afe6af7eb773d))

### [1.90.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.90.0...v1.90.1) (2021-12-16)


### Bug Fixes

* No time estimate for scripture readings ([16892a8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/16892a88b8defe2c0a243e607e2982102e0d95b3))

## [1.90.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.89.0...v1.90.0) (2021-12-16)


### Features

* Allow opening attached images in new tab ([a3f8e7a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a3f8e7acab59630ac46b947db04b663704660d25))


### Bug Fixes

* Image route forces downloading image ([64db3b3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/64db3b31c9594e2db9a846b68501ea631508b478))

## [1.89.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.88.1...v1.89.0) (2021-12-16)


### Features

* Add appointment address to funeral records ([c388895](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c38889568d6ec6eabf45e84f4ed68a01030bba1e))

### [1.88.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.88.0...v1.88.1) (2021-12-16)


### Bug Fixes

* Funeral appointment timezone issues ([abe3d45](https://codeberg.org/pfarrplaner/pfarrplaner/commits/abe3d452ff866865a827271ab90df6e55b2038ea)), closes [#149](https://codeberg.org/pfarrplaner/pfarrplaner/issues/149)
* Remove unnecessary TimeFields component from BlockEditor ([ae08a24](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ae08a24ba7fbe702e367eda9c9fd9d4e65085860))
* Start and end dates for registration not shown in ServiceEditor ([75bc65b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/75bc65b0941dc5bce9b88652b82ef090d11eb837))
* Typo in TextStats component ([35ecb3d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/35ecb3d6ca5f1152e2dac9412e02649af27899c5))

## [1.88.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.87.0...v1.88.0) (2021-12-07)


### Features

* Allow configuring rounded times and speech velocity ([623869f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/623869f77b1a6070e9cde56d29f31a66d52dc43a))
* Handle inertia requests in SettingsController ([e649aa7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e649aa7e608b48f4df04ceccd8c17bd9c5ae1c2a))


### Bug Fixes

* Cannot retrieve setting for other user ([39b8099](https://codeberg.org/pfarrplaner/pfarrplaner/commits/39b80998a208e3c065462fbba726249509a2abec))

## [1.87.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.86.0...v1.87.0) (2021-12-06)


### Bug Fixes

* Add missing time fields to other LiturgyItem types ([6e15eab](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6e15eab374cb444e016bc7ac8423b72aba5f3239))

## [1.86.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.85.0...v1.86.0) (2021-12-06)


### Features

* new KonfiApp brand asset ([#168](https://codeberg.org/pfarrplaner/pfarrplaner/issues/168)) ([120408a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/120408ae1965fd9538880a2c0a143ab14341d704))
* Show timing info in LiturgyTree ([8f1a3f7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8f1a3f76da467bb5d8455918e476edbe5f643b74))

## [1.85.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.84.0...v1.85.0) (2021-12-04)


### Features

* Provide time calc for all liturgy text fields + story field in funeral editor ([39ea3ae](https://codeberg.org/pfarrplaner/pfarrplaner/commits/39ea3ae1a8e901f58531f0999ed3476e52b628ed)), closes [#173](https://codeberg.org/pfarrplaner/pfarrplaner/issues/173)

## [1.84.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.83.0...v1.84.0) (2021-12-02)


### Features

* Add controlled access info to newsletter report ([9cf6064](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9cf60646e6104df72aed8e1c723d36eb0b598460))

## [1.83.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.82.3...v1.83.0) (2021-12-02)


### Features

* Allow specifying controlled access level (3G, 2G, 2G+, ...) for services ([7ecd97b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7ecd97bdb06f9d390829e6ba2ffb35b4bc03b5ab))

### [1.82.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.82.2...v1.82.3) (2021-11-26)


### Bug Fixes

* ServiceUpdated fails due to missing import ([844f9c3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/844f9c37a7bfa07c7c42767fe51d05a65ee8c655))

### [1.82.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.82.1...v1.82.2) (2021-11-18)


### Bug Fixes

* Failure to parse registration start/end dates ([c391dec](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c391dec85ac7815965e51e2e4ddf24e7d49a30c2))

### [1.82.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.82.0...v1.82.1) (2021-11-14)


### Features

* Prevent opening PlanningInputForm without any ministries selected ([57e6a67](https://codeberg.org/pfarrplaner/pfarrplaner/commits/57e6a67812c6f71b9822544be14e99f9cd901bbd))


### Bug Fixes

* Debug output prevents saving services ([d1333a9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d1333a9ae4a7bac0e11d290e0888a8cdae4bb47c))

## [1.82.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.81.1...v1.82.0) (2021-11-13)


### Features

* Completely rewrote PlanningInput ([3504396](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3504396c9578f275f1d3860ae10135bfabc74981))

### [1.81.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.81.0...v1.81.1) (2021-10-30)


### Features

* Improved cache-busting for JS ([5038cc2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5038cc2e615224fec2bff5b5808f0cb90c7f9cff))

## [1.81.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.80.0...v1.81.0) (2021-10-30)


### Features

* Highlight recipient name in instruction text ([75d0e9e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/75d0e9e80a3cf95d2214530da9dfd7b73ac87b2c))
* More options for A4WordSpecificLiturgySheet ([71fa6a8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/71fa6a87c1c70631aa973f0cced4e6a65b76b7bd))


### Bug Fixes

* Item recipients should be individual persons ([05e3932](https://codeberg.org/pfarrplaner/pfarrplaner/commits/05e3932a84037584c2476ffcb4ab3f7ca83fed7b))

## [1.80.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.79.3...v1.80.0) (2021-10-29)


### Features

* Add A4WordSpecificLiturgySheet ([97bf6f5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/97bf6f5248bf7e5eb262e8b83c7c481747bdae84))
* Implement first password change view in Vue ([3f6247b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3f6247b7b07e260c165e762d0338b49472df1dde)), closes [#159](https://codeberg.org/pfarrplaner/pfarrplaner/issues/159)
* Show alert in NextServicesTab when no services are planned ([a45c400](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a45c40029bb7f84034f05ea159aa8e9bbd06e95d)), closes [#160](https://codeberg.org/pfarrplaner/pfarrplaner/issues/160)


### Bug Fixes

* Add missing name attribute ([26213f1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/26213f1e468415ef74c8fe5ce8ab4a37c6468189))
* Use less data for filling import source list ([b1970d1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b1970d1ea2a59f974e9a2ac9b703f30067ce69df)), closes [#166](https://codeberg.org/pfarrplaner/pfarrplaner/issues/166)
* Use Microsoft 365-based exchange server for sync ([63d934e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/63d934e7e6866468911c35703b267c0e98cd4b9e)), closes [#162](https://codeberg.org/pfarrplaner/pfarrplaner/issues/162)

### [1.79.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.79.2...v1.79.3) (2021-10-29)


### Bug Fixes

* AbstractLiturgySheet throws an exception for guest user ([2a4f9b2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2a4f9b2793401ea4810333553cf3f690eb85b453))
* Menu not closable in mobile view ([deef512](https://codeberg.org/pfarrplaner/pfarrplaner/commits/deef512936c81d47e49a88c4c53774c3815bbcd4)), closes [#161](https://codeberg.org/pfarrplaner/pfarrplaner/issues/161)
* Reactivity problems on mobile calendar view ([2ca6a52](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2ca6a52636bd638bd920cd07fa2d7c58bfe92ba8))
* Redirect to empty screen after password change ([3a18949](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3a189494fc9f6cbee016d878d49c7ddcb8ba6f3b))
* Use cache-busting for bundled JS ([06a5870](https://codeberg.org/pfarrplaner/pfarrplaner/commits/06a5870cf5ff93c2d7495591fc0626f62ba0e555)), closes [#164](https://codeberg.org/pfarrplaner/pfarrplaner/issues/164)

### [1.79.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.79.1...v1.79.2) (2021-10-19)


### Features

* Allow adding instructions to FullTextLiturgySheet ([432b323](https://codeberg.org/pfarrplaner/pfarrplaner/commits/432b3239780590a78ea8149661103db4f2ed449e))
* Allow adding instructions to FullTextLiturgySheet ([80dc111](https://codeberg.org/pfarrplaner/pfarrplaner/commits/80dc111c5233f214b31b15129d6cc0fd663c7792))


### Bug Fixes

* Assigned ministries do not show up in PeoplePane ([2a7a32d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2a7a32d73dffebf7121f33200bb08bf9d65bb0b1))
* Cannot create new wedding records ([3e1e81b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3e1e81be5f1b11a089d51ce0ee5f3efeb4192460))

### [1.79.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.79.0...v1.79.1) (2021-10-08)


### Bug Fixes

* Array defaults should be set with factory function ([429b69f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/429b69f195231c85239e8d4619b2c3e8dc4334dd))
* Gracefully handle null entries ([814c636](https://codeberg.org/pfarrplaner/pfarrplaner/commits/814c636cb2a013fcc665eaf94c0f48812e3d996a))

## [1.79.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.9...v1.79.0) (2021-10-08)


### Features

* Update PeopleSelect architecture to work with teams ([c201777](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c201777b38bb511e0ab1b538f5bcf02218b65fa6))

### [1.78.9](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.8...v1.78.9) (2021-10-07)


### Bug Fixes

* Hide edit buttons for non-editable services ([9989a0a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9989a0a5c3e3438279cf5ac586c0d9ea48617cc1))
* Service edit form shows no location if user has no rights for the city (but may be pastor/...) ([27dcecc](https://codeberg.org/pfarrplaner/pfarrplaner/commits/27dceccc43705a99198f5dafebbfda62a6349768))
* Show own rites from any church ([84da979](https://codeberg.org/pfarrplaner/pfarrplaner/commits/84da979d000392b40f98035733aa5057210326d6))
* Show own services from any church ([f294873](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f294873d254eb3634bd077d9a8f9f43dc56f7bad))

### [1.78.8](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.7...v1.78.8) (2021-10-07)


### Features

* Allow creation of teams to group users together ([4001796](https://codeberg.org/pfarrplaner/pfarrplaner/commits/40017969d0146c77d3614c028e60598e8570dd1a))


### Bug Fixes

* Cannot change password from form ([1c036be](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1c036beaf5646d339136769c88bd5fdea1b5a7ac))
* Setting HomeScreenTabs not possible for new users ([02b2828](https://codeberg.org/pfarrplaner/pfarrplaner/commits/02b28289944ba3e03ab0296b59af9e46bed82685))
* User creation fails ([2b9b299](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2b9b2990be2b1eb205024fa5e64e215ba2fc8049))
* User creation fails because breadcrumb is missing ([7172f18](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7172f18a527d86200c5d23100f057bc2af31bd8c))

### [1.78.7](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.6...v1.78.7) (2021-10-01)


### Bug Fixes

* AnnouncementsReport fails when certain fields are not set ([d06a716](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d06a716693676edddaaaf5dfffc120a797001f01))
* SongSheetLiturgySheet fails on empty psalm/song items ([251f1bf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/251f1bf8f3ce0f9db54cd43a813e1934f6aa9c0f))

### [1.78.6](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.5...v1.78.6) (2021-09-30)


### Bug Fixes

* Delete baptism request fails because service is null ([9a1bbf9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9a1bbf90a128c9806b449b6835e4a0cab520a190))

### [1.78.5](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.4...v1.78.5) (2021-09-17)


### Bug Fixes

* Service ical returns error 404 ([27bbded](https://codeberg.org/pfarrplaner/pfarrplaner/commits/27bbded93a060ab539f4c62ddbc9541a949b6d8b))

### [1.78.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.3...v1.78.4) (2021-09-13)


### Bug Fixes

* PeoplePane crashes on some empty items ([daa37e7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/daa37e7356cb327cb8667365d5c1b75d623fbfad))

### [1.78.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.2...v1.78.3) (2021-09-10)


### Bug Fixes

* KonfiAppIntegration to throws an error when a service didn't have a time before saving. ([c632f72](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c632f720d3754d7e0f29817f8d53796450d9c63f))
* Query error in MinistryRequestReport ([d263180](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d263180d4a2123191ae65b36eb155b475d1f3dea))

### [1.78.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.1...v1.78.2) (2021-08-29)


### Features

* Display version info when hovering over app icon ([1d3a1b2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1d3a1b25333f540000ab242864f58f535c96132a))

### [1.78.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.78.0...v1.78.1) (2021-08-29)


### Bug Fixes

* Some browsers have /home route cached and now produce error 404 ([7588511](https://codeberg.org/pfarrplaner/pfarrplaner/commits/758851199b8607916da206ddbbaabf7b9e8bb2b2))
* Wizards etc. create new services without setting slug ([285ab45](https://codeberg.org/pfarrplaner/pfarrplaner/commits/285ab456df0f9ca27afe52290b769a073cfc2e8a))

## [1.78.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.77.1...v1.78.0) (2021-08-27)


### Features

* Add more options for case-related HomeScreenTabs (reverse sorting, exclusion of processed items) ([1ea51aa](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1ea51aa573e69300a6f2d64360041c0c1b3e2d43))

### [1.77.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.77.0...v1.77.1) (2021-08-26)


### Features

* enable inertia linking for submenu items ([b141130](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b1411300cf3df711780f4736bd356a7acd27a580))

## [1.77.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.76.2...v1.77.0) (2021-08-26)


### Features

* Add default service type for KonfiApp ([3f68423](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3f68423b9a90ebb7364c7f3bd3268129fd1cfeb7))
* Allow downloading existing file from FormImageAttacher ([1520ba2](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1520ba291340c6a99b9d0b957518f6c3e8a1569f))
* Allow free-text entries for responsible persons in LiturgyEditor ([e0ba041](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e0ba041ee49791f48e1edbac3afa104c979408aa))
* Automatically set labelField as searchField in FormSelectize ([b072b9c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b072b9c74af1a923d93beaae8d93a3046133c0c3))
* HandlesAttachedImageTrait now handles multiple image fields per model ([b70fa56](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b70fa56df196b4d893fa5cae1372bbdb9a9cea84))


### Bug Fixes

* Typo in trait name for HandlesAttachedImageTrait ([2bd59f1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2bd59f1e884e9cd28cb12d6bba1347e3a1de92ee))
* user.profile route not working ([e8ae1f0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e8ae1f02c12478c4696bb687a158c29d19134000))

### [1.76.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.76.1...v1.76.2) (2021-08-26)


### Bug Fixes

* Dependencies need updating for PHP 8.x ([790779e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/790779ef5a2d827d8bce64589d01412d5d4f6595))
* wrong routes to LiturgyEditor ([dae8ee6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/dae8ee60b8472cc0f20e71e544eb07ca36498071))

### [1.76.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.76.0...v1.76.1) (2021-08-26)


### Bug Fixes

* Cannot login because of empty CSRF token ([92c4d61](https://codeberg.org/pfarrplaner/pfarrplaner/commits/92c4d61d0e4c85d945965fe55ab5c75270573944))

## [1.76.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.75.0...v1.76.0) (2021-08-25)


### Features

* Allow overloading auto-created announcement file ([cba6c5c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cba6c5c352a690bd17572b6c95d6bec940949df8))
* Show warnings when auto-created announcements are overwritten ([de3c18a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/de3c18a413a609c3cfa2d3c5588c753ed5162e52))

## [1.75.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.74.1...v1.75.0) (2021-08-25)


### Features

* Add configuration options for PPT export ([a078406](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a0784067b484cca6ee0f8364ca6420dcc9530c99))
* Specify more finely-grained permissions in case of "fremden-urlaub-bearbeiten" ([8a99e3b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8a99e3bd03699d7155b094de68daab84bcc0773e)), closes [#134](https://codeberg.org/pfarrplaner/pfarrplaner/issues/134)


### Bug Fixes

* Logout sometimes doesn't close session ([ac47000](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ac47000894493d4a70183a4c07e044505d2be930))
* Session not flushed on logout ([46811c1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/46811c15989714a59f36c5d16ca7f632d3a3f060))
* wrong route ([5301e5f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5301e5f7017414545946e0d6c87cdf27097342f8))

### [1.74.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.74.0...v1.74.1) (2021-08-25)


### Features

* Include Laravel/PHP version info in about page ([1dd54af](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1dd54af3117097f172a48239c71d5552c303bc8d))


### Bug Fixes

* Wrong redirection after login / auth routes ([94e4523](https://codeberg.org/pfarrplaner/pfarrplaner/commits/94e4523343c6135bbe75883dd6653e221fbc653b))

## [1.74.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.73.0...v1.74.0) (2021-08-25)


### Features

* Add copyright to songsheet, full text output ([1f5b733](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1f5b733952f320944c41d831dbafda8d0f379f43)), closes [#100](https://codeberg.org/pfarrplaner/pfarrplaner/issues/100)
* Include song copyrights in PPT ([6fd46a3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6fd46a368b5fcff2c828d38cc8489ba4b9abc1d7)), closes [#100](https://codeberg.org/pfarrplaner/pfarrplaner/issues/100)
* Rewrote youtube:unpublish command to only touch broadcasts connected to a known service ([805588b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/805588b38178a4208cdbab3fc9a59f5fb4382495))


### Bug Fixes

* Absences link should be called with inertia ([3240650](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3240650e96283a282320ccb467ee9c51f9feb83f))
* FullTextLiturgySheet fails when song data is not present ([6e46866](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6e46866bc8f6b9befd445b283f60e03aef725c0f))
* prevent duplicate key error message in CasesTab ([513498e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/513498e3dfe43b50e613fce3af10fe57ff2a9f84))
* Relative date in funeral liturgy is relative to current day, not funeral day ([78f499e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/78f499e86803a38d2ba6b2027416eb5fa18c68eb)), closes [#128](https://codeberg.org/pfarrplaner/pfarrplaner/issues/128)
* Remove reference to obsolete whatsnew component ([299cd50](https://codeberg.org/pfarrplaner/pfarrplaner/commits/299cd50c0432241a7809bc093df4ac61290b807f))
* scopeWritable does not correctly suppy city ids ([c54b074](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c54b074645a20a8f8ddacbac6f591f648106d1f4))
* uncoupling a sermon leads to error 404 ([db1f3f7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/db1f3f7abe929f9d1f933d796ca36ca2704316ac))
* UserController crashes on edit action ([17e6c87](https://codeberg.org/pfarrplaner/pfarrplaner/commits/17e6c874f3008cc900f0e4d3fa70568264f7d87b))

## [1.73.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.72.3...v1.73.0) (2021-08-24)


### Features

* Add birth name in FuneralEditor ([9f92802](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9f928025374606cad2a5750eb4df6c8f04bca617)), closes [#136](https://codeberg.org/pfarrplaner/pfarrplaner/issues/136)
* Add common text templates to story field in FuneralEditor ([4594502](https://codeberg.org/pfarrplaner/pfarrplaner/commits/45945022b51637610597d906655d92b40ac21a14))
* Add service button to insert bible text in SermonEditor ([f2b7450](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f2b7450bd3525abc3e23fd85a79601f6538b3390)), closes [#78](https://codeberg.org/pfarrplaner/pfarrplaner/issues/78)
* New image upload in SermonEditor ([dae5bc9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/dae5bc9f286f5c0235d9b3abeff4d9cd7be1a066)), closes [#117](https://codeberg.org/pfarrplaner/pfarrplaner/issues/117) [#103](https://codeberg.org/pfarrplaner/pfarrplaner/issues/103)
* Use drag handles in LiturgyTree to improve handling on touch screens ([8d1e049](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8d1e049bd1c699ef2023dae38c781a97c1abb2c0)), closes [#137](https://codeberg.org/pfarrplaner/pfarrplaner/issues/137)

### [1.72.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.72.2...v1.72.3) (2021-08-23)


### Bug Fixes

* PsalmEditor does not save selected psalm ([8ae95b3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8ae95b34f3782c5b6491b03ca3452b19e828033c))

### [1.72.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.72.1...v1.72.2) (2021-08-20)


### Features

* Calendar always shows full name ([a23e4f7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a23e4f7dbb4da175e8105556597e4e4178028ff4)), closes [#132](https://codeberg.org/pfarrplaner/pfarrplaner/issues/132)


### Bug Fixes

* Auto-select first baptism/funeral/wedding when rendering LiturgicItems where no association is selected ([a08146f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a08146f45e48c73aa1f63c84fc3ce083e0ccd7b5)), closes [#89](https://codeberg.org/pfarrplaner/pfarrplaner/issues/89)
* Empty screen after login when certain settings are not set ([6e06e2e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6e06e2e014289472650211f89c20f27d579aa959)), closes [#130](https://codeberg.org/pfarrplaner/pfarrplaner/issues/130) [#131](https://codeberg.org/pfarrplaner/pfarrplaner/issues/131)

### [1.72.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.72.0...v1.72.1) (2021-08-18)


### Bug Fixes

* Timezone error in Funeral list view ([7b9f603](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7b9f603ed83fedea4f54b302f8e46fb07b8a3e23))

## [1.72.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.71.0...v1.72.0) (2021-08-18)


### Features

* Add announcements as automated download ([d5a212c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d5a212c623a4735f0b5acdadc59d7b85c3cea550))


### Bug Fixes

* External calendars do not update when only a rite record is edited ([0c17ef3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0c17ef3d5193f2a142342acc32e53c940c80272e))

## [1.71.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.70.0...v1.71.0) (2021-08-18)


### Features

* Add dimissorial fields to baptisms, weddings ([1a613da](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1a613da5fcd9fa22b9490dfa443b118016e05e0c)), closes [#109](https://codeberg.org/pfarrplaner/pfarrplaner/issues/109)
* Dimissorials can be granted online via a secure url ([ce60ef9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ce60ef970e0b0a70533df9abb883134a07199cf9))
* Mark rites as processed ([8ab6987](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8ab69876ea671029a56f2f559d184b7e91df55f7))


### Bug Fixes

* Checkboxes with isCheckedItem flag do not update correctly on input ([360ff2c](https://codeberg.org/pfarrplaner/pfarrplaner/commits/360ff2ca97747ae252b18e5a1673fa4687d8bfe2))
* Timezone and formatting problems with wedding appointment ([48f7737](https://codeberg.org/pfarrplaner/pfarrplaner/commits/48f773740dff77014768da7d32eae17139eddd34))

## [1.70.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.69.2...v1.70.0) (2021-08-17)


### Features

* Bible text field is now present in all rites ([d4698a4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d4698a42ceb5e7170d8c3459d6aea64055522f61))
* New vue-based WeddingEditor ([ec42b4f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ec42b4f5294ff6c009c5a74e45674b70698b69de))
* Publicly viewable plan for individual ministries ([14c3752](https://codeberg.org/pfarrplaner/pfarrplaner/commits/14c37520ef4774433f44f3da612373695b8dfd15))


### Bug Fixes

* Tests do not work any more on Laravel 8.x ([72a5852](https://codeberg.org/pfarrplaner/pfarrplaner/commits/72a58520b9e32e3f4731512daa406e18ea0d333b))

### [1.69.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.69.1...v1.69.2) (2021-08-12)


### Bug Fixes

* exchange sync fails when root calendar folder is selected as target ([14e0fe1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/14e0fe10d3f3124dc278e2f38f8bf07d9d4ac788))

### [1.69.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.69.0...v1.69.1) (2021-08-12)


### Bug Fixes

* Root calendar does not show up in exchange calendar list ([f4ebf8a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f4ebf8a911ea4ff531cdee270a546e883aa2bbe3)), closes [#127](https://codeberg.org/pfarrplaner/pfarrplaner/issues/127)

## [1.69.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.68.0...v1.69.0) (2021-08-12)


### Features

* Allow drag/drop upload directly into baptism/funeral/wedding attachment lists ([943a828](https://codeberg.org/pfarrplaner/pfarrplaner/commits/943a828c48f91f6d86af3979d72fa6070898841f))
* Better use of space in Calendar ([3ce5039](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3ce5039f226842736d2dce0764eff7778a8015e2)), closes [#114](https://codeberg.org/pfarrplaner/pfarrplaner/issues/114)
* Use today button to scroll in vertical calendar view when month is current ([ffcadd6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ffcadd68d929698c3a985bd5edbb5572038a2b36))


### Bug Fixes

* BaptismsTab has extraneous header line ([eaef551](https://codeberg.org/pfarrplaner/pfarrplaner/commits/eaef551f8c124248d8aaade04ef250bef267c2be))
* Tabs try to access obsolete config structure ([f71f6eb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/f71f6eb59b80e9ce7d163bacb78614304494547f))

## [1.68.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.67.0...v1.68.0) (2021-08-12)


### Features

* Better labels in ProfileEditor ([9710d8f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9710d8f6c51e66314a80f890d1a9a3c840b741ff))


### Bug Fixes

* A4/A5 LiturgySheets crash when psalm item is empty ([5b3ff41](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5b3ff41dd4dadbf93a116982915085841226e4aa)), closes [#115](https://codeberg.org/pfarrplaner/pfarrplaner/issues/115)
* Remove duplicate route names for compliance with Laravel 7 ([9f6f8fb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/9f6f8fb837803cdc734782070ff7ac7737f2ee1b))

## [1.67.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.66.2...v1.67.0) (2021-08-12)


### Features

* Use separate queued jobs to sync/delete services on CalendarConnections ([a79f772](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a79f77250640a7672b0962efb953aaccd3aa3038))

### [1.66.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.66.1...v1.66.2) (2021-08-11)


### Bug Fixes

* Cannot use Auth::user() in queued job ([5fde117](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5fde11783aa3722692ea1450483f34ce057020e9))
* Wrong column name ([2230efd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2230efd629cc3bf471e11f3897852277afb791f2))

### [1.66.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.66.0...v1.66.1) (2021-08-11)


### Bug Fixes

* HomeScreen fails when showReplacements setting is not set ([87b34fb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/87b34fb55cd6fab3a686b4a57c315931c76f50dd))

## [1.66.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.65.2...v1.66.0) (2021-08-11)


### Features

* Allow export to Exchange and Sharepoint calendars ([41d1d5f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/41d1d5f2d45e1a2937fa2c8aafa4987f5db86360))
* Allow export to Exchange and Sharepoint calendars ([93b7818](https://codeberg.org/pfarrplaner/pfarrplaner/commits/93b7818118248f7dec7525f71403d81fe3f8af0f))
* New inertia-based UI for user profile ([93b126d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/93b126d35b378974fad03194b0a20688dec178fe))
* Show current replacements on HomeScreen ([bdf3e47](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bdf3e47003d00827a74690a89ea053a56134c0a0)), closes [#124](https://codeberg.org/pfarrplaner/pfarrplaner/issues/124) [#125](https://codeberg.org/pfarrplaner/pfarrplaner/issues/125)


### Bug Fixes

* Add missing certificates ([0a34e45](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0a34e45663c8b23b54ac0cef0d68b94f8a7a9e09))
* Add missing disabled state to TabHeader ([ce43743](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ce43743e7ff44fc092f33d42b82d7d82ac65c789))
* HomeScreen does not set activeTab correctly ([cb65e86](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cb65e867e660ae18c4b129b28a11bb938b55c330))
* Remove non-functional UI for CalendarConnections from user profile ([8dab1a1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8dab1a1400bbb468fc93cc7d31456b2729ca534e))
* SongEditor modal dialog is not wide enough ([b562dd7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/b562dd7eb7ac3d8fac573ab878e144d9010aaec1)), closes [#118](https://codeberg.org/pfarrplaner/pfarrplaner/issues/118)
* wrong composer dependency for staudenmeir/belongs-to-through ([c3c1d90](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c3c1d904b7f033274e55ca558d16d6a3819ec70f))

### [1.65.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.65.1...v1.65.2) (2021-08-09)


### Bug Fixes

* MinistryRow does not select newly-created ministries in dropdown ([830d5b1](https://codeberg.org/pfarrplaner/pfarrplaner/commits/830d5b1c77388913832171f0204d2fd991d7d9ab))

### [1.65.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.65.0...v1.65.1) (2021-08-08)


### Features

* Add edit button to FuneralInfoPane ([ca3fd53](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ca3fd53d1c43d9fa1834f1fc07a61c0b51fabfde)), closes [#116](https://codeberg.org/pfarrplaner/pfarrplaner/issues/116)


### Bug Fixes

* A5LiturgySheet fails when songbook is not correctly set on a song/psalm ([6379abb](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6379abbfef0baf6bc0a1682ecaa0ad6e3d592bbb))
* FullTextLiturgySheet produces invalid document when text contains ampersand ([a452828](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a4528282448ad523067e1d601b91dce62e733794))
* Relative address fields lose focus after each character input ([6b82914](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6b829148ccecd75292bb883ce2ef483c6b41f21e)), closes [#71](https://codeberg.org/pfarrplaner/pfarrplaner/issues/71)

## [1.65.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.64.0...v1.65.0) (2021-07-09)


### Features

* Output SongSheetLiturgySheet as a word document ([7da72b7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7da72b76b17e55c65036d5275f7d4eb3900f7ffa))
* Set document properties on FullTextLiturgySheet, SongSheetLiturgySheet ([c5486e3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c5486e34032c2d988c39a327a34f2b92487559b6))


### Bug Fixes

* Revert csrf timeout changes (caused 419 errors) ([a89a488](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a89a488cbfab45c3609f5d2ae58194df69c36bff))

## [1.64.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.63.0...v1.64.0) (2021-07-05)


### Features

* CommuniApp integration updated to respect publishing delays ([804b120](https://codeberg.org/pfarrplaner/pfarrplaner/commits/804b1209d3d2ccecde586d86dc5634e8f6b5b121))


### Bug Fixes

* cc_alt_time input has wrong format, producing validation errors ([a390d9d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/a390d9d1fe0ca91dd767a563f888e50f378ddf89))
* Debug logging produces error on CommuniApp updates ([75bdb4f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/75bdb4f412f89466a3424acf9f3f0f7108092ad8))
* Forms expire when tab remains inactive too long ([59f6a97](https://codeberg.org/pfarrplaner/pfarrplaner/commits/59f6a97684ef73c00ca1e76420f86cd1787abf2d))

## [1.63.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.62.0...v1.63.0) (2021-07-02)


### Features

* Add TravelRequestFormReport ([2bbda0d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2bbda0de30e01ea2dad18ba9ebc65bb6b70d9b27))
* Show button for vacation request in AbsencesTab ([1bcf7b5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1bcf7b5caf7c397a3ca65e6bbb1af46fc987c4b7))

## [1.62.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.61.0...v1.62.0) (2021-07-01)


### Features

* Automatically create a vacation request form for an absence ([6bb381f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6bb381ff46de2d5710de9f88dc98ea7722074a24))


### Bug Fixes

* cannot delete attached baptisms/funerals/weddings ([431e6af](https://codeberg.org/pfarrplaner/pfarrplaner/commits/431e6af057d917dbc31f6d89c1f505040c48b4cf))
* Date range input missing on MultipleServicesInput ([c51d013](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c51d01306e0d83893d8e1cef288157a276664da6))
* KonfiAppIntegration fails to save new qr code ([2f91efd](https://codeberg.org/pfarrplaner/pfarrplaner/commits/2f91efd0ef19d99b0c91699a4dde07783378bee1))
* Newly created baptisms are not pre-associated to the correct city ([c8b229d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c8b229dc4f7d7139760187b4fe6e1ad83bb6e1a1))
* validation errors do not show ([156fb1b](https://codeberg.org/pfarrplaner/pfarrplaner/commits/156fb1bfdad95cc40317d0607823748e2010cac6))

## [1.61.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.60.5...v1.61.0) (2021-06-25)


### Features

* Add FuneralInfoPane in LiturgyEditor ([e1f2bb5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/e1f2bb55b821d9ae0db04bef94ac706145bb8719))
* Add image preview to Attachment ([0639c20](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0639c2059bf66403e6d464e1fef0fbf843c2ef8d)), closes [#90](https://codeberg.org/pfarrplaner/pfarrplaner/issues/90)
* Add relative date for dod to FuneralEditor ([7389e9a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7389e9adf306cd1a64842e6b103019449a2d4300)), closes [#110](https://codeberg.org/pfarrplaner/pfarrplaner/issues/110)


### Bug Fixes

* Wrong links ([5773a11](https://codeberg.org/pfarrplaner/pfarrplaner/commits/5773a115b29f5a7cd344437f71dd54b2d5912e73))

### [1.60.5](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.60.4...v1.60.5) (2021-06-17)


### Features

* Allow selecting existing sermon in LiturgyTree ([1b75f93](https://codeberg.org/pfarrplaner/pfarrplaner/commits/1b75f930d9990b06d9672170aad4a62d083a4f58)), closes [#102](https://codeberg.org/pfarrplaner/pfarrplaner/issues/102)
* File upload via drag and drop ([8e8b077](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8e8b077c2f8ea9f898c19059015be1e91aafe8e5))
* Show a marker in LiturgyTree when an item has personalized data ([c6943f8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c6943f8214656bd767cccf6f271d877304020343)), closes [#107](https://codeberg.org/pfarrplaner/pfarrplaner/issues/107)
* Show available markers in FreetextEditor ([7bdb29a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7bdb29a465c5acd91a942c9433207b850ef36348)), closes [#104](https://codeberg.org/pfarrplaner/pfarrplaner/issues/104)
* Users with write access are able to create/edit locations for a city ([cdd695d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cdd695d18e4a1f2513615f5973206f67fd334d29)), closes [#106](https://codeberg.org/pfarrplaner/pfarrplaner/issues/106)

### [1.60.4](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.60.3...v1.60.4) (2021-06-17)


### Features

* Show pastor in cases overview on HomeScreen ([bea8452](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bea8452f341ee6dfb743dba287ffc39e4ae32adc))


### Bug Fixes

* BaptismEditor does not update service_id ([67df91e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/67df91e03129e7807590f528ccd1c7a901bbc55b))
* downloading attachment from baptism request fails with error 403 ([027068d](https://codeberg.org/pfarrplaner/pfarrplaner/commits/027068dc4bc2cdc1387fa3b5b8b554bd299c4a2f))

### [1.60.3](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.60.2...v1.60.3) (2021-06-14)


### Features

* Show pastor on homescreen overviews ([c959957](https://codeberg.org/pfarrplaner/pfarrplaner/commits/c9599575377ab85deaf79b5241d0097d43340430))


### Bug Fixes

* Baptisms/funerals/weddings from other cities appear on homescreen ([620939a](https://codeberg.org/pfarrplaner/pfarrplaner/commits/620939aeaf540a911794936ba0bba400f8c890be))
* NextOfferingsTab is empty ([140bce5](https://codeberg.org/pfarrplaner/pfarrplaner/commits/140bce5ede5950928c2232ca5802b27d24db6825))

### [1.60.2](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.60.1...v1.60.2) (2021-06-11)


### Bug Fixes

* BaptismEditor won't save correctly ([6185af7](https://codeberg.org/pfarrplaner/pfarrplaner/commits/6185af7814d91faf504edaa82d03d7ac206ae5f7))
* Wizard buttons should use Inertia for baptism/funeral ([0e381d3](https://codeberg.org/pfarrplaner/pfarrplaner/commits/0e381d3b055c0bf812874e372953e83487b4b3b6))

### [1.60.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.60.0...v1.60.1) (2021-06-11)


### Features

* Additional liturgical info and copyable bible references in LiturgyEditor ([d959a96](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d959a9605c6cec560c963057511b10714286ba4f))
* Show first line of FreeText items ([8c92664](https://codeberg.org/pfarrplaner/pfarrplaner/commits/8c92664490bc131c98028fb207eeccb476024162))


### Bug Fixes

* ReferenceParser always returns single verse ranges ([d9417d9](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d9417d95acfbcc1fc206b86e3bbb28140bea12d4))

## [1.60.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.59.0...v1.60.0) (2021-06-06)


### Features

* Additional liturgical info and copyable bible references in LiturgyEditor ([091c4a6](https://codeberg.org/pfarrplaner/pfarrplaner/commits/091c4a69e40d658f99426824abd6a3ba9d17d260))


### Bug Fixes

* Multiple fixes to ReferenceParser and BibleText services ([cd966ba](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cd966ba28c45bf4b2037d8701e7d91b36692e0f2))
* ReferenceParser fails when first verse range is optional () ([db8815e](https://codeberg.org/pfarrplaner/pfarrplaner/commits/db8815ea9534d32438cc86d4b75dd63a759c5e43))

## [1.59.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.58.1...v1.59.0) (2021-06-06)


### Features

* Add link to Youtube's livestreaming dashboard to StreamingTab ([976fd7f](https://codeberg.org/pfarrplaner/pfarrplaner/commits/976fd7f4158ae303980eb29d05ea651ca6e64d2b))
* LiturgySheet configuration moved to modal dialogs ([ab263c4](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ab263c41a0868881722c5ffe79dc10d607618e21))


### Bug Fixes

* AttachmentTab doesn't use Inertia for calling LiturgySheet configuration ([3a91a94](https://codeberg.org/pfarrplaner/pfarrplaner/commits/3a91a9475f3b5c38efb28b2b52cce804fb9a0702))
* Created broadcasts have no streaming key ([47f7ecf](https://codeberg.org/pfarrplaner/pfarrplaner/commits/47f7ecf1c4d06b435bfbaf32e2a7638f87330c4f))
* Deleting wrong verses in SongEditor ([cc40417](https://codeberg.org/pfarrplaner/pfarrplaner/commits/cc40417feed060becebca00df206f825d6f14480)), closes [#85](https://codeberg.org/pfarrplaner/pfarrplaner/issues/85)
* Modal dialogs default width does not scale on mobile devices ([7eb8c44](https://codeberg.org/pfarrplaner/pfarrplaner/commits/7eb8c44d16431136aa19ae440f5e8aa816011115))
* youtube:update command does not set streaming key when previous streaming is null ([aa68ac0](https://codeberg.org/pfarrplaner/pfarrplaner/commits/aa68ac0244ee30aeb600b7345d73a98466cc32fb))

### [1.58.1](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.58.0...v1.58.1) (2021-06-05)


### Bug Fixes

* FormCheck does not handle 0 values correctly ([bad74b8](https://codeberg.org/pfarrplaner/pfarrplaner/commits/bad74b84fb6762883874ae7b7be057dcc274997f))
* LiturgySheet configuration does not correctly remember user settings ([ee5e770](https://codeberg.org/pfarrplaner/pfarrplaner/commits/ee5e770e3209cebf7d995b961175556ed3fd9533))

## [1.58.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.57.0...v1.58.0) (2021-06-05)


### Features

* LiturgySheets (here: FullTextLiturgySheet) can optionally be configured ([d1d24ff](https://codeberg.org/pfarrplaner/pfarrplaner/commits/d1d24ff19b3bbd2b4bd2effe2480b6f0e681516e))

## [1.57.0](https://codeberg.org/pfarrplaner/pfarrplaner/compare/v1.56.0...v1.57.0) (2021-06-05)


### Bug Fixes

* Manual paths need to be relative in order to work on GitHub ([908c7ea](https://codeberg.org/pfarrplaner/pfarrplaner/commits/908c7ea960a394afb455d8cda33ec2f1413471db))
* Prevent attempt to unserialize twice ([98c03ff](https://codeberg.org/pfarrplaner/pfarrplaner/commits/98c03ff4ebac1c03180810f83776433bb5a94ef0))

## [1.56.0](https://github.com/potofcoffee/pfarrplaner/compare/v1.55.4...v1.56.0) (2021-06-05)


### Features

* Add manual builder command ([366ce5c](https://github.com/potofcoffee/pfarrplaner/commits/366ce5c78dd3dcf8e1526449c63a68b116339556))
* Allow linking to .md file paths ([b17a263](https://github.com/potofcoffee/pfarrplaner/commits/b17a2631c8a3139ee7a202b52d3d94bb5e4fbfd9))
* Basic infrastructure for an online manual ([3391ba4](https://github.com/potofcoffee/pfarrplaner/commits/3391ba4f097ba2c3fa9e32b1b1c8d385db8a48d2))
* Manual builder now creates TOC and moves images ([0b7015f](https://github.com/potofcoffee/pfarrplaner/commits/0b7015f3e0a49b15b0bc011f321e31d7400f5fc5))


### Bug Fixes

* HelpLayout should show TOC link with text ([130d8c4](https://github.com/potofcoffee/pfarrplaner/commits/130d8c40c614b258ad2e1bf20e48a97610846fb1))
* ServiceRequest fails validation when special_location field is not present ([d5cbaed](https://github.com/potofcoffee/pfarrplaner/commits/d5cbaed4d8531c148d841c4f9fbfded6abc0fbc1))
* wrong image path in manual media ([fcbef40](https://github.com/potofcoffee/pfarrplaner/commits/fcbef40355ed285cdaa9a175f940349bfce559e0))

### [1.55.4](https://github.com/potofcoffee/pfarrplaner/compare/v1.55.3...v1.55.4) (2021-06-04)


### Bug Fixes

* AbsenceEditor fails to save replacements ([ac24879](https://github.com/potofcoffee/pfarrplaner/commits/ac24879a7f31334b6b6378ceac4d7aa948cd960f))
* DateRangeInput does not set end of range ([ba2e73c](https://github.com/potofcoffee/pfarrplaner/commits/ba2e73c0f82114d69798f3fe6f3eff0ff89ccae5))
* Planner does not show replacements ([f1c9867](https://github.com/potofcoffee/pfarrplaner/commits/f1c98671ed272699a51a9f1f918b8f4587be4e43))

### [1.55.3](https://github.com/potofcoffee/pfarrplaner/compare/v1.55.2...v1.55.3) (2021-06-03)


### Features

* Automatically create CREDITS.md file ([9d438a7](https://github.com/potofcoffee/pfarrplaner/commits/9d438a7a13a846916ab446b85dc36d4236bed435))
* Show depencency links in CREDITS.md ([74eca03](https://github.com/potofcoffee/pfarrplaner/commits/74eca03fc732c4e6c6424870a7507f266602afb5))

### [1.55.2](https://github.com/potofcoffee/pfarrplaner/compare/v1.55.1...v1.55.2) (2021-06-03)

### [1.55.1](https://github.com/potofcoffee/pfarrplaner/compare/v1.55.0...v1.55.1) (2021-06-03)


### Features

* Show environment type in version info ([ac4c581](https://github.com/potofcoffee/pfarrplaner/commits/ac4c5811c056b4c056473290c1f8b6c967ad7f0d))


### Bug Fixes

* Check for registration document is shown twice in BaptismEditor ([8a020c2](https://github.com/potofcoffee/pfarrplaner/commits/8a020c2f895ea093fd6064d652e15cd8c0955aa8))

## [1.55.0](https://github.com/mokkapps/changelog-generator-demo/compare/v1.54.0...v1.55.0) (2021-06-03)


### Features

* Add new about info with changelog ([88ca927](https://github.com/mokkapps/changelog-generator-demo/commits/88ca927040dfa6b07df955133dfdfa1214307fa2))

## 1.54.0 (2021-06-03)


### Features

* Add changelog generation and semantic versioning ([146fb4e](https://github.com/mokkapps/changelog-generator-demo/commits/146fb4e1a23ed3ab5461e02272eba43237e80ea8))


### Bug Fixes

* Place blocking fails when field is empty ([4647d3d](https://github.com/mokkapps/changelog-generator-demo/commits/4647d3d51635f0a4554462f6d67f02d4232b9587))
