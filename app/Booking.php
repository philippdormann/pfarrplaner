<?php

namespace App;

use App\Casts\EncryptedAttribute;
use AustinHeap\Database\Encryption\Traits\HasEncryptedAttributes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $fillable = [
        'service_id',
        'code',
        'name',
        'first_name',
        'contact',
        'number',
        'fixed_seat',
        'override_seats',
        'override_split',
        'email',
    ];

    protected $casts = [
        'name' => EncryptedAttribute::class,
        'first_name' => EncryptedAttribute::class,
        'contact' => EncryptedAttribute::class,
        'email' => EncryptedAttribute::class,
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('number', 'desc');
        });
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service() {
        return $this->belongsTo(Service::class);
    }

    public static function createCode() {
        return str_pad(dechex(rand(0x100000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT);
    }

}
