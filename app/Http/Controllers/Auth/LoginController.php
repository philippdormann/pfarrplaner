<?php
/**
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarrplaner/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Auth;

use App\City;
use App\Http\Controllers\Controller;
use App\Service;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Illuminate\Http\Request;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'keepTokenAlive']);
    }

    /**
     * @return Application|Factory|View
     */
    public function showLoginForm(Request $request)
    {
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        $demo = (app()->environment() == 'demo');
        $users = $demo ? User::with('roles', 'homeCities')->where('password', '!=', '')->orderBy('id')->get() : [];
        $blog = null;
        if ($url = (config('blog.blog_feed', false))) {
            $blog = simplexml_load_file($url);

            if (count($blog->channel->item) > 3) {
                $columns = 3;
            }
        }

        try {
            $ytFeed = (array)simplexml_load_file(config('support.youtube_channel_feed'));
            $videos = [];
            foreach ($ytFeed['entry'] as $video) {
                $videos[(string)$video->title] = str_replace(
                    'https://www.youtube.com/watch?v=',
                    'https://www.youtube.com/embed/',
                    (string)$video->link->attributes()->href
                );
            }
        } catch (\Exception $exception) {
            $videos = [];
        }


        $count = [
            'cities' => City::count(),
            'users' => User::where('password', '!=', '')->count(),
            'services' => Service::count(),
        ];

        $packageConfig = json_decode(file_get_contents(base_path('package.json')), true);
        $version = $packageConfig['version'];

        $recaptchaKey = config('recaptcha.key');

        return view('auth.login', compact('blog', 'videos', 'count', 'version', 'demo', 'users', 'recaptchaKey'));
    }

    public function setInitialPassword()
    {
    }

    public function keepTokenAlive()
    {
        return response()->json(['token' => csrf_token()]);
    }

}
